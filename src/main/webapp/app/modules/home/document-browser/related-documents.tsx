import React, { useEffect, useState } from 'react';
import { IDocument } from 'app/shared/model/document.model';
import { useHistory, useLocation } from 'react-router-dom';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { getSortState, JhiItemCount, JhiPagination } from 'react-jhipster';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { Row, ListGroup, ListGroupItem } from 'reactstrap';
import { InputRegistry } from 'app/shared/layout/inputs/input-registry';

interface IProps {
  totalItems: number;
  templateId: number;
  relatedDocuments: readonly IDocument[];
  onRelatedDocumentClick: (id: number) => void;
  getRelatedDocuments: (templateId: number, page?: number, size?: number, sort?: string) => void;
}

export const RelatedDocuments = (props: IProps) => {
  const location = useLocation();
  const history = useHistory();
  const [pagination, setPagination] = useState(
    overridePaginationStateWithQueryParams(getSortState(location, ITEMS_PER_PAGE), location.search)
  );

  useEffect(() => {
    if (!props.templateId) {
      return;
    }

    props.getRelatedDocuments(
      props.templateId,
      pagination.activePage - 1,
      pagination.itemsPerPage,
      `${pagination.sort},${pagination.order}`
    );
    const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;

    if (location.search !== endURL) {
      history.push(`${location.pathname}${endURL}`);
    }
  }, [props.templateId, pagination.activePage, pagination.order, pagination.sort]);

  useEffect(() => {
    setPagination({
      ...pagination,
      ...overridePaginationStateWithQueryParams(pagination, location.search)
    });
  }, [location.search]);

  const handlePagination = currentPage =>
    setPagination({
      ...pagination,
      activePage: currentPage,
    });

  const handleDocumentClick = (id: number) => () => {
    props.onRelatedDocumentClick(id);
  };

  return (
    <div>
      <ListGroup>
        {props.relatedDocuments.map((document: IDocument) => (
          <ListGroupItem
            key={document.id}
            className='related-document-item'
            tag='button'
            onClick={handleDocumentClick(document.id)}
            action
          >
            ID: {document.id}

            <div className='d-flex align-items-center ml-4'>
              {document.defaultCustomFields?.map(field => (
                <div key={field.id} className='mr-1'>
                  <InputRegistry field={field} disabled inlineLabel/>
                </div>
              ))}
            </div>
          </ListGroupItem>
        ))}
      </ListGroup>

      {props.totalItems > 0 &&
        <div className={props.relatedDocuments?.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount
              page={pagination.activePage}
              total={props.totalItems}
              itemsPerPage={pagination.itemsPerPage}
              i18nEnabled
            />
          </Row>

          <Row className="justify-content-center">
            <JhiPagination
              activePage={pagination.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={pagination.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      }
    </div>
  );
};
