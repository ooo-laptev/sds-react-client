import React, { useEffect, useMemo, useState } from 'react';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from 'reactstrap';
import { IDocument } from 'app/shared/model/document.model';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ITemplateLayout } from 'app/shared/model/template.model';
import { InputRegistry } from 'app/shared/layout/inputs/input-registry';

interface IProps {
  isOpen: boolean;
  templateId: number;
  templateLayout: ITemplateLayout;
  document?: IDocument;
  getTemplateForm: (templateId: number, documentId?: number) => void;
  saveTemplateForm: (formValues: ITemplateLayout, documentId?: number) => void;
  deleteDocument: (id: number, templateId?: number) => void;
  onClose: () => void;
}

export const DocumentProcessingModal = (props: IProps) => {
  const [activeTab, setActiveTab] = useState<string>();
  const [formLayout, setFormLayout] = useState<ITemplateLayout>(props.templateLayout);
  const [isFormDisabled, setIsFormDisabledFlag] = useState<boolean>(true);
  const [isNew, setIsNew] = useState<boolean>(false);

  useEffect(() => {
    if (props.isOpen) {
      props.getTemplateForm(props.templateId, props.document?.id);

      const isNewFlag = !props.document?.id;

      setIsFormDisabledFlag(!isNewFlag);
      setIsNew(isNewFlag);
    }
  }, [props.isOpen]);

  useEffect(() => {
    if (props.templateLayout?.content) {
      setFormLayout(props.templateLayout);
      setActiveTab(props.templateLayout.content[0]?.name);
    }
  }, [props.templateLayout]);

  const toggle = tab => () => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const handleClose = event => {
    event.stopPropagation();
    props.onClose();
  };

  const handleInputChange = (blockId: number) => (id: number, value, error?: string) => {
    const newFormLayout: ITemplateLayout = {
      ...formLayout,
      content: formLayout.content.map(block => block.id !== blockId
        ? block
        : {
          ...block,
          content: block.content.map(field => field.id === id ? {...field, value, error} : field)
        }
      )
    };

    setFormLayout(newFormLayout);
  };

  const isSaveBtnDisabled = useMemo(() => {
    return formLayout.content?.some(block => block.content?.some(field => field.error));
  }, [formLayout]);

  const handleSaveClick = () => {
    props.saveTemplateForm(formLayout, props.document?.id);
    setIsFormDisabledFlag(true);

    if (isNew) {
      props.onClose();
    }
  };

  const handleDeleteClick = () => {
    props.deleteDocument(props.document?.id, props.templateId);
    props.onClose();
  };

  const getModalButtons = () => {
    if (isNew) {
      return (
        <Button disabled={isSaveBtnDisabled} color="success" onClick={handleSaveClick}>
          <Translate contentKey="entity.action.create">Create</Translate>
        </Button>
      );
    }

    return (
      <>
        <Button color="danger" onClick={handleDeleteClick}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>

        { isFormDisabled ?
          <Button color="secondary" onClick={() => setIsFormDisabledFlag(false)}>
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </Button>
          :
          <Button disabled={isSaveBtnDisabled} color="success" onClick={handleSaveClick}>
            <Translate contentKey="entity.action.save">Save</Translate>
          </Button>
        }
      </>
    );
  };

  return (
    <Modal isOpen={props.isOpen} toggle={handleClose} className='document-processing-modal'>
      <ModalHeader toggle={handleClose}>
        {!isNew ? `ID: ${props.document?.id}` : 'N/A'}
      </ModalHeader>

      <ModalBody>
        <Nav tabs>
          {formLayout.content?.map(block => (
            <NavItem key={block.name}>
              <NavLink
                className={activeTab === block.name ? 'active' : ''}
                onClick={toggle(block.name)}
              >
                {block.name}
              </NavLink>
            </NavItem>
          ))}
        </Nav>

        <TabContent activeTab={activeTab} className='tab-content-wrapper'>
          {formLayout.content?.map(block => (
            <TabPane key={block.name} tabId={block.name}>
              {block.content?.map(field => (
                <div key={field.name} className='input-registry-wrapper'>
                  <InputRegistry field={field} onChange={handleInputChange(block.id)} disabled={isFormDisabled} />
                </div>
              ))}
            </TabPane>
          ))}
        </TabContent>
      </ModalBody>

      <ModalFooter>
        {getModalButtons()}
      </ModalFooter>
    </Modal>
  );
};
