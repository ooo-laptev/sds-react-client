import React, { useEffect, useMemo, useState } from 'react';
import './document-browser.scss';
import {
  Col,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  ListGroup,
  ListGroupItem,
  Row,
  UncontrolledDropdown
} from 'reactstrap';
import {
  getTemplates,
  reset as resetTemplateStore
} from 'app/modules/administration/sds-configuration/template-management/template-management.reducer';
import { RelatedDocuments } from 'app/modules/home/document-browser/related-documents';
import { DocumentProcessingModal } from 'app/modules/home/document-browser/document-processing-modal';
import {
  deleteDocument,
  getRelatedDocuments,
  getTemplateForm,
  reset,
  saveTemplateForm
} from 'app/modules/home/document-browser/document-browser.reducer';
import { Translate } from 'react-jhipster';
import { ActionModal, ActionModalTypeEnum } from 'app/modules/home/document-browser/action-modal';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { ITemplateLayout } from 'app/shared/model/template.model';

interface ActionModalData {
  isOpen: boolean;
  type?: ActionModalTypeEnum;
}

const DocumentBrowser = () => {
  const dispatch = useAppDispatch();
  const [currentTemplateId, setCurrentTemplateId] = useState<number>(0);
  const [currentDocumentId, setCurrentDocumentId] = useState<number>(0);
  const [actionModalData, setActionModalData] = useState<ActionModalData>({isOpen: false});
  const [isDocProcessingModalOpen, setIsDocProcessingModalOpen] = useState<boolean>(false);

  const templates = useAppSelector(store => store.templateManagement.templates);
  const templateLayout = useAppSelector(store => store.documentBrowser.templateLayout);
  const relatedDocuments = useAppSelector(store => store.documentBrowser.documents);
  const totalItems = useAppSelector(store => store.documentBrowser.documentTotalItems);

  useEffect(() => {
    dispatch(getTemplates({}));

    return () => {
      dispatch(reset());
      dispatch(resetTemplateStore());
    }
  }, []);

  const handleTemplateClick = (id: number) => () => {
    setCurrentTemplateId(id);
  };

  const handleActionClick = (type: ActionModalTypeEnum) => () => {
    setActionModalData({isOpen: true, type});
  };

  const handleSelectTemplate = (templateId: number) => {
    setCurrentTemplateId(templateId);
    setIsDocProcessingModalOpen(true);
  };

  const handleGetRelatedDocuments = (templateId: number, page?: number, size?: number, sort?: string) => {
    dispatch(getRelatedDocuments({templateId, page, size, sort}));
  };

  const handleRelatedDocumentClick = (id: number) => {
    setCurrentDocumentId(id);
    setIsDocProcessingModalOpen(true);
  };

  const handleDocProcessingModalClose = () => {
    setCurrentDocumentId(0);
    setIsDocProcessingModalOpen(false);
    dispatch(getRelatedDocuments({templateId: currentTemplateId}));
  };

  const calculatedDocument = useMemo(() => {
    return relatedDocuments.find(document => document.id === currentDocumentId);
  }, [currentDocumentId]);

  const handleGetTemplateForm = (templateId: number, documentId?: number) => {
    dispatch(getTemplateForm({templateId, documentId}));
  };

  const handleSaveTemplateForm = (formValues: ITemplateLayout, documentId?: number) => {
    dispatch(saveTemplateForm({formValues, documentId}));
  };

  const handleDeleteDocument = (id: number, templateId?: number) => {
    dispatch(deleteDocument({id, templateId}));
  };

  return (
    <div className='document-browser'>
      <Row className='mb-3'>
        <Col md='7'>
          {templates.find(template => template.id === currentTemplateId)?.name}
        </Col>

        <Col md='5' className='d-flex justify-content-end'>
          <UncontrolledDropdown>
            <DropdownToggle caret>
              <Translate contentKey='documentBrowser.actions.title'>Actions</Translate>
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem onClick={handleActionClick(ActionModalTypeEnum.CREATE_DOCUMENT)}>
                <Translate contentKey='documentBrowser.actions.createDocument'>Create document</Translate>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>

          <ActionModal
            isOpen={actionModalData.isOpen}
            type={actionModalData.type}
            currentTemplateId={currentTemplateId}
            templates={templates}
            onSelectTemplate={handleSelectTemplate}
            onClose={() => setActionModalData({isOpen: false})}
          />
        </Col>
      </Row>

      <Row>
        <Col md='3'>
          <ListGroup>
            {templates.map(template => (
              <ListGroupItem
                key={template.id}
                tag='button'
                active={template.id === currentTemplateId}
                onClick={handleTemplateClick(template.id)}
                action
              >
                {template.name}
              </ListGroupItem>
            ))}
          </ListGroup>
        </Col>

        <Col md='9'>
          <RelatedDocuments
            templateId={currentTemplateId}
            relatedDocuments={relatedDocuments}
            totalItems={totalItems}
            onRelatedDocumentClick={handleRelatedDocumentClick}
            getRelatedDocuments={handleGetRelatedDocuments}
          />

          <DocumentProcessingModal
            isOpen={isDocProcessingModalOpen}
            templateId={currentTemplateId}
            document={calculatedDocument}
            templateLayout={templateLayout}
            getTemplateForm={handleGetTemplateForm}
            saveTemplateForm={handleSaveTemplateForm}
            deleteDocument={handleDeleteDocument}
            onClose={handleDocProcessingModalClose}
          />
        </Col>
      </Row>
    </div>
  );
};

export default DocumentBrowser;
