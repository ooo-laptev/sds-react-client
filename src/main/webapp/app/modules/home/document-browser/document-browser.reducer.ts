import { IDocument, IDocumentList } from 'app/shared/model/document.model';
import axios from 'axios';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { defaultValue, ITemplateLayout } from 'app/shared/model/template.model';
import { createAsyncThunk, createSlice, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit';
import { serializeAxiosError } from 'app/shared/reducers/reducer.utils';

const initialState = {
  loading: false,
  errorMessage: null,
  documents: [] as ReadonlyArray<IDocument>,
  documentTotalItems: 0,
  templateLayout: defaultValue
};

export type DocumentBrowserState = Readonly<typeof initialState>;

// Actions

export const getRelatedDocuments = createAsyncThunk(
  'documentBrowser/FETCH_DOCUMENTS',
  (data: {templateId?: number; page?: number; size?: number; sort?: string}) => {
    let page = data.page;
    let size = data.size;
    let sort = data.sort;

    if (!page && !size && !sort) {
      const params = Object.fromEntries(new URLSearchParams(window.location.search));
      sort = params.sort;
      page = +params.page - 1;
      size = ITEMS_PER_PAGE;
    }

    const sortParams = sort ? `&page=${page}&size=${size}&sort=${sort}` : '';
    const requestUrl = `/api/documentBrowser/documents?currentTemplateId=${data.templateId}${sortParams}`;

    return axios.get<IDocumentList>(requestUrl);
  }
);

export const getTemplateForm = createAsyncThunk(
  'documentBrowser/FETCH_TEMPLATE_FORM',
  (data: {templateId: number; documentId?: number}) => {
    const requestUrl = `/api/getForm?templateId=${data.templateId}${data.documentId ? `&documentId=${data.documentId}` : ''}`;
    return axios.get<ITemplateLayout>(requestUrl);
});

export const saveTemplateForm = createAsyncThunk(
  'documentBrowser/SAVE_TEMPLATE_FORM',
  async (data: {formValues: ITemplateLayout; documentId?: number}, thunkAPI) => {
    const requestUrl = `/api/saveFormValues${data.documentId ? `?documentId=${data.documentId}` : ''}`;

    const result = await axios.post<ITemplateLayout>(requestUrl, data.formValues);

    thunkAPI.dispatch(getTemplateForm({templateId: data.formValues.id, documentId: data.documentId}));

    if (!data.documentId) {
      thunkAPI.dispatch(getRelatedDocuments({templateId: data.formValues.id}));
    }

    return result;
  },
  { serializeError: serializeAxiosError }
);

export const deleteDocument = createAsyncThunk(
  'documentBrowser/DELETE_DOCUMENT',
  async (data: {id: number; templateId?: number}, thunkAPI) => {
    const result = await axios.delete(`api/documents/${data.id}`);

    thunkAPI.dispatch(getRelatedDocuments({templateId: data.templateId}));
    return result;
  },
  { serializeError: serializeAxiosError }
);

export const DocumentBrowserSlice = createSlice({
  name: 'documentBrowser',
  initialState: initialState as DocumentBrowserState,
  reducers: {
    reset() {
      return initialState;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getRelatedDocuments.fulfilled, (state, action) => {
        state.loading = false;
        state.documents = action.payload.data.content;
        state.documentTotalItems = action.payload.data.totalElements;
      })
      .addMatcher(isFulfilled(getTemplateForm, saveTemplateForm), (state, action) => {
        state.loading = false;
        state.templateLayout = action.payload.data;
      })
      .addMatcher(isPending(getRelatedDocuments, getTemplateForm, saveTemplateForm), (state) => {
        state.errorMessage = null;
        state.loading = true
      })
      .addMatcher(isRejected(getRelatedDocuments, getTemplateForm, saveTemplateForm), (state, action) => {
        state.loading = false;
        state.errorMessage = action.payload
      })
  }
});

export const { reset } = DocumentBrowserSlice.actions;

export default DocumentBrowserSlice.reducer;
