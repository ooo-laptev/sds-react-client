import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Label } from 'reactstrap';
import { AvField, AvForm, AvGroup } from 'availity-reactstrap-validation';
import { Translate } from 'react-jhipster';
import { ITemplate } from 'app/shared/model/template.model';

interface IProps {
  isOpen: boolean;
  currentTemplateId?: number;
  templates: readonly ITemplate[];
  type: ActionModalTypeEnum;
  onSelectTemplate: (templateId: number) => void;
  onClose: () => void;
}

export enum ActionModalTypeEnum {
  CREATE_DOCUMENT = 'CREATE_DOCUMENT'
}

export const ActionModal = (props: IProps) => {
  const handleClose = event => {
    event.stopPropagation();
    props.onClose();
  };

  const handleSelectTemplate = (event, values) => {
    props.onSelectTemplate(values.templateId);
    props.onClose();
  };

  if (props.type === ActionModalTypeEnum.CREATE_DOCUMENT) {
    return (
      <Modal isOpen={props.isOpen} toggle={handleClose}>
        <ModalHeader toggle={handleClose}>
          <Translate contentKey='documentBrowser.actions.createDocument'>Create document</Translate>
        </ModalHeader>

        <AvForm onValidSubmit={handleSelectTemplate}>
          <ModalBody>
            <AvGroup>
              <Label for="assignedTemplates">
                <Translate contentKey="documentManagement.properties.assignedTemplates">Template</Translate>
              </Label>
              <AvField
                type="select"
                className="form-control"
                name="templateId"
                value={props.currentTemplateId || props.templates[0]?.id}
              >
                {props.templates.map(template => (
                  <option key={template.id} title={template.description} value={template.id}>{template.name}</option>
                ))}
              </AvField>
            </AvGroup>
          </ModalBody>

          <ModalFooter>
            <Button color="secondary" type="submit">
              <Translate contentKey="entity.action.select">Select</Translate>
            </Button>
          </ModalFooter>
        </AvForm>
      </Modal>
    );
  }

  return null;
};
