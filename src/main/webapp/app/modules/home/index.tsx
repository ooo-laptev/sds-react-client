import React from 'react';
import { connect } from 'react-redux';
import { AUTHORITIES } from 'app/config/constants';
import Welcome from 'app/modules/home/welcome/welcome';
import DocumentBrowser from 'app/modules/home/document-browser/document-browser';
import {IRootState} from "app/config/store";

type IProps = StateProps;

const Home = (props: IProps) => {
  return (
    props.account.authorities?.includes(AUTHORITIES.ADMIN)
      ? <DocumentBrowser />
      : <Welcome />
  );
};

const mapStateToProps = (store: IRootState) => ({
  account: store.authentication.account,
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
