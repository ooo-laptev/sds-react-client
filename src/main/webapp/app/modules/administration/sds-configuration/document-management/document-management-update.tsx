import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Label, Row } from 'reactstrap';
import { AvField, AvForm, AvGroup } from 'availity-reactstrap-validation';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { createDocument, getDocument, reset, updateDocument } from './document-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { getTemplates } from 'app/modules/administration/sds-configuration/template-management/template-management.reducer';

export const DocumentManagementUpdate = (props: RouteComponentProps<{ documentId: string }>) => {
  const dispatch = useAppDispatch();
  const [isNew] = useState(!props.match.params || !props.match.params.documentId);

  const templates = useAppSelector(store => store.templateManagement.templates);
  const document = useAppSelector(store => store.documentManagement.document);
  const loading = useAppSelector(store => store.documentManagement.loading);
  const updating = useAppSelector(store => store.documentManagement.updating);

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getDocument(props.match.params.documentId));
    }

    if (!templates.length) {
      dispatch(getTemplates({}));
    }

    return () => {
      dispatch(reset());
    };
  }, []);

  const handleClose = () => {
    props.history.push('/admin/sds-configuration/document-management');
  };

  const saveDocument = (event, values) => {
    values.assignedTemplates = [+values.assignedTemplates];

    if (isNew) {
      dispatch(createDocument(values));
    } else {
      dispatch(updateDocument(values));
    }
    handleClose();
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1>
            <Translate contentKey="documentManagement.home.createOrEditLabel">Create or edit a Document</Translate>
          </h1>
        </Col>
      </Row>

      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm onValidSubmit={saveDocument}>
              {!!document.id &&
                <AvGroup>
                  <Label for="id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvField type="text" className="form-control" name="id" required readOnly value={document.id} />
                </AvGroup>
              }

              <AvGroup>
                <Label for="created">
                  <Translate contentKey="documentManagement.properties.created">Created</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="created"
                  value={document.created}
                />
              </AvGroup>

              <AvGroup>
                <Label for="assignedTemplates">
                  <Translate contentKey="documentManagement.properties.assignedTemplates">Template</Translate>
                </Label>
                <AvField
                  type="select"
                  className="form-control"
                  name="assignedTemplates"
                  value={document.assignedTemplates[0] || templates[0]?.id}
                >
                  {templates.map(template => (
                    <option key={template.id} title={template.description} value={template.id}>{template.name}</option>
                  ))}
                </AvField>
              </AvGroup>

              <Button tag={Link} to="/admin/sds-configuration/document-management" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>

              &nbsp;

              <Button color="primary" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default DocumentManagementUpdate;
