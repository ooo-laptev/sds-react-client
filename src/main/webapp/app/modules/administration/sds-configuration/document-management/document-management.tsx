import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table } from 'reactstrap';
import { getSortState, JhiItemCount, JhiPagination, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { getDocuments } from './document-management.reducer';
import { getTemplates } from 'app/modules/administration/sds-configuration/template-management/template-management.reducer';
import { IDocument } from 'app/shared/model/document.model';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const DocumentManagement = (props: RouteComponentProps) => {
  const dispatch = useAppDispatch();
  const [pagination, setPagination] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE), props.location.search)
  );

  useEffect(() => {
    dispatch(
      getDocuments({page: pagination.activePage - 1, size: pagination.itemsPerPage, sort: `${pagination.sort},${pagination.order}`})
    );
    const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;

    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  }, [pagination.activePage, pagination.order, pagination.sort]);

  useEffect(() => {
    dispatch(getTemplates({}));
  }, []);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPagination({
        ...pagination,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });

  const handlePagination = currentPage =>
    setPagination({
      ...pagination,
      activePage: currentPage,
    });

  const documents = useAppSelector(store => store.documentManagement.documents);
  const totalItems = useAppSelector(store => store.documentManagement.totalItems);
  const templates = useAppSelector(store => store.templateManagement.templates);
  const { match } = props;

  return (
    <div>
      <h2 id="document-management-page-heading">
        <Translate contentKey="documentManagement.home.title">Documents</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
          <FontAwesomeIcon icon="plus" /> &nbsp;

          <Translate contentKey="documentManagement.home.createLabel">
            Create a new document
          </Translate>
        </Link>
      </h2>
      <Table responsive striped>
        <thead>
          <tr>
            <th className="hand" onClick={sort('id')}>
              <Translate contentKey="global.field.id">ID</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
            <th className="hand" onClick={sort('created')}>
              <Translate contentKey="documentManagement.properties.created">Created</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
            <th className="hand" onClick={sort('assignedTemplates')}>
              <Translate contentKey="documentManagement.properties.assignedTemplates">Template</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
          </tr>
        </thead>
        <tbody>
          {documents.map((document: IDocument, i: number) => (
            <tr id={String(document.id)} key={`document-${i}`}>
              <td>
                <Button tag={Link} to={`${match.url}/${document.id}`} color="link" size="sm">
                  {document.id}
                </Button>
              </td>
              <td>{document.created}</td>
              <td>
                {document.assignedTemplates?.map(id =>
                  templates.find(template => template.id === id)?.name).join(', ')
                }
              </td>

              <td className="text-right">
                <div className="btn-group flex-btn-group-container">
                  <Button tag={Link} to={`${match.url}/${document.id}`} color="info" size="sm">
                    <FontAwesomeIcon icon="eye" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.view">View</Translate>
                    </span>
                  </Button>
                  <Button tag={Link} to={`${match.url}/${document.id}/edit`} color="primary" size="sm">
                    <FontAwesomeIcon icon="pencil-alt" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.edit">Edit</Translate>
                    </span>
                  </Button>
                  <Button
                    tag={Link}
                    to={`${match.url}/${document.id}/delete`}
                    color="danger"
                    size="sm"
                  >
                    <FontAwesomeIcon icon="trash" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.delete">Delete</Translate>
                    </span>
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {totalItems > 0 &&
        <div className={documents && documents.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={pagination.activePage} total={totalItems} itemsPerPage={pagination.itemsPerPage} i18nEnabled />
          </Row>

          <Row className="justify-content-center">
            <JhiPagination
              activePage={pagination.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={pagination.itemsPerPage}
              totalItems={totalItems}
            />
          </Row>
        </div>
      }
    </div>
  );
};

export default DocumentManagement;
