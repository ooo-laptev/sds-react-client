import axios from 'axios';
import { defaultValue, IDocument } from 'app/shared/model/document.model';
import { createAsyncThunk, createSlice, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit';
import { IQueryParams, serializeAxiosError } from 'app/shared/reducers/reducer.utils';

const initialState = {
  loading: false,
  errorMessage: null,
  documents: [] as ReadonlyArray<IDocument>,
  authorities: [] as any[],
  document: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0,
};

export type DocumentManagementState = Readonly<typeof initialState>;

// Actions
const apiUrl = 'api/documents';

export const getDocuments = createAsyncThunk(
  'documentManagement/FETCH_DOCUMENTS',
  async ({ page, size, sort }: IQueryParams
  ) => {
    const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
    return axios.get<IDocument[]>(requestUrl);
  });

export const getDocument = createAsyncThunk('documentManagement/FETCH_DOCUMENT', async (id: string) => {
  return axios.get<IDocument>(`${apiUrl}/${id}`);
});

export const createDocument = createAsyncThunk(
  'documentManagement/CREATE_DOCUMENT',
  async (document: IDocument, thunkAPI) => {
    const response = await axios.post(apiUrl, document);
    thunkAPI.dispatch(getDocuments({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

export const updateDocument = createAsyncThunk(
  'documentManagement/UPDATE_DOCUMENT',
  async (document: IDocument, thunkAPI) => {
    const response = await axios.put(apiUrl, document);
    thunkAPI.dispatch(getDocuments({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

export const deleteDocument = createAsyncThunk(
  'documentManagement/DELETE_DOCUMENT',
  async (id: string, thunkAPI) => {
    const response = await axios.delete(`${apiUrl}/${id}`);
    thunkAPI.dispatch(getDocuments({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

export const DocumentManagementSlice = createSlice({
  name: 'documentManagement',
  initialState: initialState as DocumentManagementState,
  reducers: {
    reset() {
      return initialState;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getDocuments.fulfilled, (state, action) => {
        state.loading = false;
        state.documents = action.payload.data;
        state.totalItems = parseInt(action.payload.headers['x-total-count'], 10);
      })
      .addCase(getDocument.fulfilled, (state, action) => {
        state.loading = false;
        state.document = action.payload.data;
      })
      .addCase(deleteDocument.fulfilled, (state) => {
        state.loading = false;
        state.updateSuccess = true;
        state.document = defaultValue;
      })
      .addMatcher(isFulfilled(createDocument, updateDocument), (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.document = action.payload.data;
      })
      .addMatcher(
        isPending(
          getDocuments,
          getDocument,
          createDocument,
          updateDocument,
          deleteDocument,
        ),
        (state) => {
          state.errorMessage = null;
          state.updateSuccess = false;
          state.loading = true;
        }
      )
      .addMatcher(
        isRejected(
          getDocuments,
          getDocument,
          createDocument,
          updateDocument,
          deleteDocument,
        ),
        (state, action) => {
          state.loading = false;
          state.updating = false;
          state.updateSuccess = false;
          state.errorMessage = action.payload;
        }
      )
  }
});

export const { reset } = DocumentManagementSlice.actions;

export default DocumentManagementSlice.reducer;
