import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { deleteDocument, getDocument } from './document-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const DocumentManagementDeleteDialog = (props: RouteComponentProps<{ documentId: string }>) => {
  const dispatch = useAppDispatch();
  const document = useAppSelector(store => store.documentManagement.document);

  useEffect(() => {
    dispatch(getDocument(props.match.params.documentId));
  }, []);

  const handleClose = event => {
    event.stopPropagation();
    props.history.push('/admin/sds-configuration/document-management');
  };

  const confirmDelete = event => {
    dispatch(deleteDocument(document.id.toString()));
    handleClose(event);
  };

  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>
      <ModalBody>
        <Translate contentKey="documentManagement.delete.question" interpolate={{ name: document.id }}>
          Are you sure you want to delete this Document?
        </Translate>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>
        <Button color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default DocumentManagementDeleteDialog;
