import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getDocument } from './document-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { getTemplates } from 'app/modules/administration/sds-configuration/template-management/template-management.reducer';

export const DocumentManagementDetail = (props: RouteComponentProps<{ documentId: string }>) => {
  const dispatch = useAppDispatch();
  const templates = useAppSelector(store => store.templateManagement.templates);
  const document = useAppSelector(store => store.documentManagement.document);

  useEffect(() => {
    dispatch(getDocument(props.match.params.documentId));

    if (!templates.length) {
      dispatch(getTemplates({}));
    }
  }, []);

  return (
    <div>
      <h2>
        <Translate contentKey="documentManagement.detail.title">Document</Translate> [<b>{document.id}</b>]
      </h2>

      <Row size="md">
        <dl className="jh-entity-details">
          <dt>
            <Translate contentKey="documentManagement.properties.id">ID</Translate>
          </dt>
          <dd>
            <span>{document.id}</span>
          </dd>

          <dt>
            <Translate contentKey="documentManagement.properties.created">Created</Translate>
          </dt>
          <dd>{document.created}</dd>

          <dt>
            <Translate contentKey="documentManagement.properties.assignedTemplates">Template</Translate>
          </dt>
          <dd>
            {document.assignedTemplates?.map(id =>
              templates.find(template => template.id === id)?.name).join(', ')
            }
          </dd>
        </dl>
      </Row>

      <Button tag={Link} to="/admin/sds-configuration/document-management" replace color="info">
        <FontAwesomeIcon icon="arrow-left" />{' '}
        <span className="d-none d-md-inline">
          <Translate contentKey="entity.action.back">Back</Translate>
        </span>
      </Button>
    </div>
  );
};

export default DocumentManagementDetail;
