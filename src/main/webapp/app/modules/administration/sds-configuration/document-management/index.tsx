import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import DocumentManagement from './document-management';
import DocumentManagementDetail from './document-management-detail';
import DocumentManagementUpdate from './document-management-update';
import DocumentManagementDeleteDialog from './document-management-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={DocumentManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:documentId/edit`} component={DocumentManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:documentId`} component={DocumentManagementDetail} />
      <ErrorBoundaryRoute path={match.url} component={DocumentManagement} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:documentId/delete`} component={DocumentManagementDeleteDialog} />
  </>
);

export default Routes;
