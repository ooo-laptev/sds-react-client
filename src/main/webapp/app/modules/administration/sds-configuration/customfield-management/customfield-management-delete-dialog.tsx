import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { deleteCustomField, getCustomField, getRelatedFieldBlocks } from './customfield-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CustomFieldManagementDeleteDialog = (props: RouteComponentProps<{ customfield: string }>) => {
  const dispatch = useAppDispatch();
  const customfield = useAppSelector(store => store.customfieldManagement.customfield);
  const totalRelatedFieldBlocks = useAppSelector(store => store.customfieldManagement.relatedFieldBlocksTotalItems);

  useEffect(() => {
    dispatch(getCustomField(props.match.params.customfield));
    dispatch(getRelatedFieldBlocks({customfieldId: props.match.params.customfield}));
  }, []);

  const handleClose = event => {
    event.stopPropagation();
    props.history.push('/admin/sds-configuration/customfield-management');
  };

  const confirmDelete = event => {
    dispatch(deleteCustomField(customfield.id.toString()));
    handleClose(event);
  };

  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>

      <ModalBody>
        <Translate contentKey="customfieldManagement.delete.question" interpolate={{ name: customfield.name }}>
          Are you sure you want to delete this CustomField?
        </Translate>

        {totalRelatedFieldBlocks > 0 &&
          <>
            <br />

            <Translate
              contentKey="customfieldManagement.delete.fieldBlocksWarning"
              interpolate={{ total: totalRelatedFieldBlocks }}
            >
              This customField will be unlinked from all fieldBlocks
            </Translate>
          </>
        }
      </ModalBody>

      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>
        <Button color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default CustomFieldManagementDeleteDialog;
