import axios from 'axios';
import { defaultValue, ICustomField } from 'app/shared/model/customfield.model';
import { IFieldBlock } from 'app/shared/model/fieldBlock.model';
import { createAsyncThunk, createSlice, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit';
import { IQueryParams, serializeAxiosError } from 'app/shared/reducers/reducer.utils';

const initialState = {
  loading: false,
  errorMessage: null,
  customfields: [] as ReadonlyArray<ICustomField>,
  authorities: [] as any[],
  customfield: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0,
  relatedFieldBlocks: [] as Readonly<IFieldBlock[]>,
  relatedFieldBlocksTotalItems: 0,
  availableTypes: [] as string[]
};

export type CustomFieldManagementState = Readonly<typeof initialState>;

// Actions
const apiUrl = 'api/customFields';

export const getCustomFields = createAsyncThunk(
  'customfieldManagement/FETCH_CUSTOMFIELDS',
  async ({ page, size, sort }: IQueryParams
) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return axios.get<ICustomField[]>(requestUrl);
});

export const getAllCustomFields = createAsyncThunk('customfieldManagement/FETCH_ALL_CUSTOMFIELDS', async () => {
  return axios.get<ICustomField[]>('/api/fieldblock/getAvailableCustomFields');
});

export const getCustomField = createAsyncThunk('customfieldManagement/FETCH_CUSTOMFIELD', async (id: string) => {
  return axios.get<ICustomField>(`${apiUrl}/${id}`);
});

export const createCustomField = createAsyncThunk(
  'customfieldManagement/CREATE_CUSTOMFIELD',
  async (customfield: ICustomField, thunkAPI) => {
    const response = await axios.post(apiUrl, customfield);
    thunkAPI.dispatch(getCustomFields({}));

    return response;
  },
  { serializeError: serializeAxiosError }
);

export const updateCustomField = createAsyncThunk(
  'customfieldManagement/UPDATE_CUSTOMFIELD',
  async (customfield: ICustomField, thunkAPI) => {
    const response = await axios.put(apiUrl, customfield);
    thunkAPI.dispatch(getCustomFields({}));

    return response;
  },
  { serializeError: serializeAxiosError }
);

export const deleteCustomField = createAsyncThunk(
  'customfieldManagement/DELETE_CUSTOMFIELD',
  async (id: string, thunkAPI) => {
    const response = await axios.delete(`${apiUrl}/${id}`);
    thunkAPI.dispatch(getCustomFields({}));

    return response;
  },
  { serializeError: serializeAxiosError }
);

interface IRelatedFieldBlocksActionData {
  customfieldId: string;
  page?: number;
  size?: number;
  sort?: string;
}

export const getRelatedFieldBlocks = createAsyncThunk(
  'customfieldManagement/FETCH_RELATED_FIELD_BLOCKS',
  async (data: IRelatedFieldBlocksActionData) => {
    const sortParams = data.sort ? `&page=${data.page}&size=${data.size}&sort=${data.sort}` : '';
    const requestUrl = `/api/getFieldBlocksRelatedWithCustomField?customFieldId=${data.customfieldId}${sortParams}`;

    return axios.get<IFieldBlock[]>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const getAvailableTypes = createAsyncThunk('customfieldManagement/FETCH_AVAILABLE_TYPES', async () => {
    return axios.get<string[]>(`/api/customFields/availableTypes`);
  },
  { serializeError: serializeAxiosError }
);

export const CustomfieldManagementSlice = createSlice({
  name: 'customfieldManagement',
  initialState: initialState as CustomFieldManagementState,
  reducers: {
    reset() {
      return initialState;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getCustomField.fulfilled, (state, action) => {
        state.loading = false;
        state.customfield = action.payload.data;
      })
      .addCase(deleteCustomField.fulfilled, (state => {
        state.loading = false;
        state.updateSuccess = true;
        state.customfield = defaultValue;
      }))
      .addCase(getRelatedFieldBlocks.fulfilled, (state, action) => {
        state.loading = false;
        state.relatedFieldBlocks = action.payload.data;
        state.relatedFieldBlocksTotalItems = parseInt(action.payload.headers['x-total-count'], 10);
      })
      .addCase(getAvailableTypes.fulfilled, (state, action) => {
        state.loading = false;
        state.availableTypes = action.payload.data;
      })
      .addMatcher(isFulfilled(getCustomFields, getAllCustomFields), (state, action) => {
        state.loading = false;
        state.customfields = action.payload.data;
        state.totalItems = parseInt(action.payload.headers['x-total-count'], 10);
      })
      .addMatcher(isFulfilled(createCustomField, updateCustomField), (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.customfield = action.payload.data;
      })
      .addMatcher(
        isPending(
          getCustomFields,
          getCustomField,
          createCustomField,
          updateCustomField,
          deleteCustomField,
          getRelatedFieldBlocks,
          getAllCustomFields
        ),
        (state) => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.loading = true;
      })
      .addMatcher(
        isRejected(
          getCustomFields,
          getCustomField,
          createCustomField,
          updateCustomField,
          deleteCustomField,
          getRelatedFieldBlocks,
          getAllCustomFields
        ),
        (state, action) => {
        state.loading = false;
        state.updating = false;
        state.updateSuccess = false;
        state.errorMessage = action.error.message;
      });
  }
});

export const { reset } = CustomfieldManagementSlice.actions;

export default CustomfieldManagementSlice.reducer;
