import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Label, Row } from 'reactstrap';
import { AvField, AvForm, AvGroup } from 'availity-reactstrap-validation';
import { translate, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  createCustomField,
  getAvailableTypes,
  getCustomField,
  reset,
  updateCustomField
} from './customfield-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { getCustomFieldKeyType, InputTypeEnum } from 'app/shared/layout/inputs/input-registry';

export const CustomFieldManagementUpdate = (props: RouteComponentProps<{ customfield: string }>) => {
  const dispatch = useAppDispatch();
  const [isNew] = useState(!props.match.params || !props.match.params.customfield);

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getCustomField(props.match.params.customfield));
    }

    dispatch(getAvailableTypes());

    return () => {
      dispatch(reset());
    };
  }, []);

  const handleClose = () => {
    props.history.push('/admin/sds-configuration/customfield-management');
  };

  const saveCustomField = (event, values) => {
    if (isNew) {
      dispatch(createCustomField(values));
    } else {
      dispatch(updateCustomField(values));
    }
    handleClose();
  };

  const isInvalid = false;
  const customfield = useAppSelector(store => store.customfieldManagement.customfield);
  const loading = useAppSelector(store => store.customfieldManagement.loading);
  const updating = useAppSelector(store => store.customfieldManagement.updating);
  const availableTypes = useAppSelector(store => store.customfieldManagement.availableTypes);
  const defaultTypeValue = availableTypes.find(type =>
    getCustomFieldKeyType(type) === InputTypeEnum.TEXT
  );

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1>
            <Translate contentKey="customfieldManagement.home.createOrEditLabel">Create or edit a
            CustomField</Translate>
          </h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm onValidSubmit={saveCustomField}>
              {!!customfield.id && (
                <AvGroup>
                  <Label for="id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvField type="text" className="form-control" name="id" required readOnly value={customfield.id} />
                </AvGroup>
              )}
              <AvGroup>
                <Label for="name">
                  <Translate contentKey="customfieldManagement.name">Name</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="name"
                  value={customfield.name}
                />
              </AvGroup>

              <AvGroup>
                <Label for="description">
                  <Translate contentKey="customfieldManagement.description">Description</Translate>
                </Label>
                <AvField
                  type="text"
                  className="form-control"
                  name="description"
                  value={customfield.description}
                />
              </AvGroup>

              <AvGroup>
                <Label for="assignedTemplates">
                  <Translate contentKey="customfieldManagement.type">Type</Translate>
                </Label>
                <AvField
                  type="select"
                  className="form-control"
                  name="type"
                  value={customfield.type || defaultTypeValue}
                >
                  {availableTypes.map(type => (
                    <option key={type} title={getCustomFieldKeyType(type)} value={type}>
                      {translate(`customfieldManagement.typeName.${getCustomFieldKeyType(type)}`)}
                    </option>
                  ))}
                </AvField>
              </AvGroup>

              <Button tag={Link} to="/admin/sds-configuration/customfield-management" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" type="submit" disabled={isInvalid || updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CustomFieldManagementUpdate;
