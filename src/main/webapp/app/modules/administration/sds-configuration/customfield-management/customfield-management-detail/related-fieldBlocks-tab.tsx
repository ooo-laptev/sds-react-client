import React, { useEffect, useState } from 'react';
import { getSortState, JhiItemCount, JhiPagination, Translate } from 'react-jhipster';
import { Button, Row, Table } from 'reactstrap';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { Link, useHistory, useLocation, useParams } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IFieldBlock } from 'app/shared/model/fieldBlock.model';

interface IProps {
  totalItems: number;
  relatedFieldBlocks: readonly IFieldBlock[];
  getRelatedFieldBlocks: (customfieldId: string, page?: number, size?: number, sort?: string) => void;
}

export const RelatedFieldBlocksTab = (props: IProps) => {
  const location = useLocation();
  const history = useHistory();
  const { customfield: customfieldId } = useParams<{customfield: string}>();
  const [pagination, setPagination] = useState(
    overridePaginationStateWithQueryParams(getSortState(location, ITEMS_PER_PAGE), location.search)
  );

  useEffect(() => {
    props.getRelatedFieldBlocks(
      customfieldId,
      pagination.activePage - 1,
      pagination.itemsPerPage,
      `${pagination.sort},${pagination.order}`
    );
    const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;

    if (location.search !== endURL) {
      history.push(`${location.pathname}${endURL}`);
    }
  }, [pagination.activePage, pagination.order, pagination.sort]);

  useEffect(() => {
    setPagination({
      ...pagination,
      ...overridePaginationStateWithQueryParams(pagination, location.search)
    });
  }, [location.search]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });

  const handlePagination = currentPage =>
    setPagination({
      ...pagination,
      activePage: currentPage,
    });

  return (
    <div>
      <Table responsive striped>
        <thead>
        <tr>
          <th className="hand" onClick={sort('ID')}>
            <Translate contentKey="global.field.id">ID</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>

          <th className="hand" onClick={sort('name')}>
            <Translate contentKey="global.field.name">Name</Translate>
            <FontAwesomeIcon icon="sort" />
          </th>
        </tr>
        </thead>

        <tbody>
        {props.relatedFieldBlocks.map(fieldBlock => (
          <tr id={String(fieldBlock.id)} key={`fieldBlock-${fieldBlock.id}`}>
            <td>
              <Button
                tag={Link}
                to={`/admin/sds-configuration/customfield-management/${fieldBlock.id}`}
                color="link"
                size="sm"
              >
                {fieldBlock.id}
              </Button>
            </td>

            <td>{fieldBlock.name}</td>
          </tr>
        ))}
        </tbody>
      </Table>

      {props.totalItems > 0 &&
      <div className={props.relatedFieldBlocks?.length > 0 ? '' : 'd-none'}>
        <Row className="justify-content-center">
          <JhiItemCount
            page={pagination.activePage}
            total={props.totalItems}
            itemsPerPage={pagination.itemsPerPage}
            i18nEnabled
          />
        </Row>

        <Row className="justify-content-center">
          <JhiPagination
            activePage={pagination.activePage}
            onSelect={handlePagination}
            maxButtons={5}
            itemsPerPage={pagination.itemsPerPage}
            totalItems={props.totalItems}
          />
        </Row>
      </div>
      }
    </div>
  );
};
