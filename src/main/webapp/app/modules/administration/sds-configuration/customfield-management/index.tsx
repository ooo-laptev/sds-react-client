import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import CustomFieldManagement from './customfield-management';
import CustomFieldManagementDetail from './customfield-management-detail/customfield-management-detail';
import CustomFieldManagementUpdate from './customfield-management-update';
import CustomFieldManagementDeleteDialog from './customfield-management-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={CustomFieldManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:customfield/edit`} component={CustomFieldManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:customfield`} component={CustomFieldManagementDetail} />
      <ErrorBoundaryRoute path={match.url} component={CustomFieldManagement} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:customfield/delete`} component={CustomFieldManagementDeleteDialog} />
  </>
);

export default Routes;
