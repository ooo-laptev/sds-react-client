import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getAvailableTypes, getCustomField, getRelatedFieldBlocks } from '../customfield-management.reducer';
import {
  RelatedFieldBlocksTab
} from 'app/modules/administration/sds-configuration/customfield-management/customfield-management-detail/related-fieldBlocks-tab';
import { GeneralTab } from 'app/modules/administration/sds-configuration/customfield-management/customfield-management-detail/general-tab';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const CustomFieldManagementDetail = (props: RouteComponentProps<{ customfield: string}>) => {
  const dispatch = useAppDispatch();
  const [activeTab, setActiveTab] = useState('general');
  const customfield = useAppSelector(store => store.customfieldManagement.customfield);
  const relatedFieldBlocksTotalItems = useAppSelector(store => store.customfieldManagement.relatedFieldBlocksTotalItems);
  const relatedFieldBlocks = useAppSelector(store => store.customfieldManagement.relatedFieldBlocks);

  useEffect(() => {
    dispatch(getCustomField(props.match.params.customfield));
    dispatch(getAvailableTypes());
  }, []);

  const toggle = tab => () => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const handleGetRelatedFieldBlocks = (customfieldId: string, page?: number, size?: number, sort?: string) => {
    dispatch(getRelatedFieldBlocks({customfieldId, page, size, sort}));
  };

  return (
    <div>
      <h2>
        <Translate contentKey="customfieldManagement.detail.title">CustomField</Translate> [<b>{customfield.name}</b>]
      </h2>
      <Nav tabs>
        <NavItem>
          <NavLink
            className={activeTab === 'general' ? 'active' : ''}
            onClick={toggle('general')}
          >
            <Translate contentKey="customfieldManagement.detail.generalTab">General</Translate>
          </NavLink>
        </NavItem>

        <NavItem>
          <NavLink
            className={activeTab === 'relatedFieldBlocks' ? 'active' : ''}
            onClick={toggle('relatedFieldBlocks')}
          >
            <Translate contentKey="customfieldManagement.detail.relatedFieldBlocksTab">Related fieldBlocks</Translate>
          </NavLink>
        </NavItem>
      </Nav>

      <TabContent activeTab={activeTab}>
        <TabPane tabId="general">
          <GeneralTab customfield={customfield} />
        </TabPane>

        <TabPane tabId="relatedFieldBlocks">
          <RelatedFieldBlocksTab
            totalItems={relatedFieldBlocksTotalItems}
            relatedFieldBlocks={relatedFieldBlocks}
            getRelatedFieldBlocks={handleGetRelatedFieldBlocks}
          />
        </TabPane>
      </TabContent>


      <Button tag={Link} to="/admin/sds-configuration/customfield-management" replace color="info">
        <FontAwesomeIcon icon="arrow-left" />{' '}
        <span className="d-none d-md-inline">
          <Translate contentKey="entity.action.back">Back</Translate>
        </span>
      </Button>
    </div>
  );
};

export default CustomFieldManagementDetail;
