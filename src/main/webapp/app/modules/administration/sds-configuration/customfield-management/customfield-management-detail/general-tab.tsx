import React from 'react';
import { IFieldBlockLayout } from 'app/shared/model/fieldBlock.model';
import { Translate } from 'react-jhipster';
import { Row } from 'reactstrap';
import { ICustomField } from 'app/shared/model/customfield.model';

interface IProps {
  customfield: ICustomField;
}

export const GeneralTab = (props: IProps) => {
  const { customfield } = props;

  return (
    <Row size="md">
      <dl className="jh-entity-details">
        <dt>
          <Translate contentKey="customfieldManagement.id">ID</Translate>
        </dt>
        <dd>
          <span>{customfield.id}</span>
        </dd>
        <dt>
          <Translate contentKey="customfieldManagement.name">Name</Translate>
        </dt>
        <dd>{customfield.name}</dd>
        <dt>
          <Translate contentKey="customfieldManagement.description">Description</Translate>
        </dt>
        <dd>{customfield.description}</dd>
        <dt>
          <Translate contentKey="customfieldManagement.type">Type</Translate>
        </dt>
        <dd>{customfield.type}</dd>
      </dl>
    </Row>
  );
};
