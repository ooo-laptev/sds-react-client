import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import TemplateManagement from './template-management';
import TemplateManagementDetail from './template-management-detail/template-management-detail';
import TemplateManagementUpdate from './template-management-update/template-management-update';
import TemplateManagementDeleteDialog from './template-management-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TemplateManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:templateId/edit`} component={TemplateManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:templateId`} component={TemplateManagementDetail} />
      <ErrorBoundaryRoute path={match.url} component={TemplateManagement} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:templateId/delete`} component={TemplateManagementDeleteDialog} />
  </>
);

export default Routes;
