import React, { useEffect, useMemo, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  addDefaultCustomField,
  addRelatedFieldBlock,
  deleteDefaultCustomField,
  deleteRelatedFieldBlock,
  getTemplate,
  getRelatedDocuments,
  moveDefaultCustomField,
  moveRelatedFieldBlock,
  reset
} from '../template-management.reducer';
import { GeneralTab } from 'app/modules/administration/sds-configuration/template-management/template-management-detail/general-tab';
import {
  RelatedDocumentsTab
} from 'app/modules/administration/sds-configuration/template-management/template-management-detail/related-documents-tab';
import { getFieldBlocks } from 'app/modules/administration/sds-configuration/fieldBlock-management/fieldBlock-management.reducer';
import { Dnd } from 'app/shared/layout/dnd/dnd';
import { ICustomField } from 'app/shared/model/customfield.model';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const TemplateManagementDetail = (props: RouteComponentProps<{ templateId: string }>) => {
  const dispatch = useAppDispatch();
  const [activeTab, setActiveTab] = useState('general');

  const template = useAppSelector(store => store.templateManagement.template);
  const relatedDocuments = useAppSelector(store => store.templateManagement.relatedDocuments);
  const relatedDocumentTotalItems = useAppSelector(store => store.templateManagement.relatedDocumentTotalItems);
  const fieldBlocks = useAppSelector(store => store.fieldBlockManagement.fieldBlocks);

  useEffect(() => {
    dispatch(getTemplate(props.match.params.templateId));

    return () => {
      dispatch(reset());
    };
  }, []);

  const toggle = tab => () => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const fullCustomFieldList = useMemo(() => {
    const result: ICustomField[] = [];

    template.content?.forEach(block => {
      block.content?.forEach(field => {
        if (!result.find(item => item.id === field.id)) {
          result.push(field);
        }
      });
    });

    return result;
  }, [template]);

  const handleGetRelatedDocuments = (templateId: string, page?: number, size?: number, sort?: string) => {
    dispatch(getRelatedDocuments({templateId, page, size, sort}));
  };

  const handleGetFieldBlocks = () => {
    dispatch(getFieldBlocks({}));
  };

  const handleRelatedFieldBlockAction = (func) => (...args) => {
    dispatch(func({templateId: args[0], fieldBlockId: args[1], position: args[2]}));
  };

  const handleDefaultCustomFieldAction = (func) => (...args) => {
    dispatch(func({templateId: args[0], customFieldId: args[1], position: args[2]}));
  };

  return (
    <div>
      <h2>
        <Translate contentKey="templateManagement.detail.title">Template</Translate> [<b>{template.id}</b>]
      </h2>

      <Nav tabs>
        <NavItem>
          <NavLink
            className={activeTab === 'general' ? 'active' : ''}
            onClick={toggle('general')}
          >
            <Translate contentKey="templateManagement.detail.generalTab">General</Translate>
          </NavLink>
        </NavItem>

        <NavItem>
          <NavLink
            className={activeTab === 'relatedDocuments' ? 'active' : ''}
            onClick={toggle('relatedDocuments')}
          >
            <Translate contentKey="templateManagement.detail.relatedDocumentsTab">Related documents</Translate>
          </NavLink>
        </NavItem>

        <NavItem>
          <NavLink
            className={activeTab === 'relatedFieldBlocks' ? 'active' : ''}
            onClick={toggle('relatedFieldBlocks')}
          >
            <Translate contentKey="templateManagement.detail.relatedFieldBlocksTab">Related fieldBlocks</Translate>
          </NavLink>
        </NavItem>

        <NavItem>
          <NavLink
            className={activeTab === 'previewCustomFields' ? 'active' : ''}
            onClick={toggle('previewCustomFields')}
          >
            <Translate contentKey="templateManagement.detail.previewCustomFields">Pre-view CustomFields</Translate>
          </NavLink>
        </NavItem>
      </Nav>

      <TabContent activeTab={activeTab}>
        <TabPane tabId="general">
          <GeneralTab template={template} />
        </TabPane>

        <TabPane tabId="relatedDocuments">
          <RelatedDocumentsTab
            totalItems={relatedDocumentTotalItems}
            relatedDocuments={relatedDocuments}
            getRelatedDocuments={handleGetRelatedDocuments}
          />
        </TabPane>

        <TabPane tabId="relatedFieldBlocks">
          <Dnd
            disabled
            entityHolder={template}
            fullEntityList={fieldBlocks}
            getFullEntityList={handleGetFieldBlocks}
            handleAddEntity={handleRelatedFieldBlockAction(addRelatedFieldBlock)}
            handleDeleteEntity={handleRelatedFieldBlockAction(deleteRelatedFieldBlock)}
            handleMoveEntity={handleRelatedFieldBlockAction(moveRelatedFieldBlock)}
          />
        </TabPane>

        <TabPane tabId="previewCustomFields">
          <Dnd
            disabled
            entityHolder={{id: template.id, content: template.defaultCustomFields || []}}
            fullEntityList={fullCustomFieldList}
            handleAddEntity={handleDefaultCustomFieldAction(addDefaultCustomField)}
            handleDeleteEntity={handleDefaultCustomFieldAction(deleteDefaultCustomField)}
            handleMoveEntity={handleDefaultCustomFieldAction(moveDefaultCustomField)}
          />
        </TabPane>
      </TabContent>

      <Button tag={Link} to="/admin/sds-configuration/template-management" replace color="info">
        <FontAwesomeIcon icon="arrow-left" />&nbsp;

        <span className="d-none d-md-inline">
          <Translate contentKey="entity.action.back">Back</Translate>
        </span>
      </Button>
    </div>
  );
};

export default TemplateManagementDetail;
