import React, { useEffect, useState } from 'react';
import { getSortState, JhiItemCount, JhiPagination, Translate } from 'react-jhipster';
import { Row, Table, Button} from 'reactstrap';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { useLocation, useHistory, Link, useParams } from 'react-router-dom';
import { IDocument } from 'app/shared/model/document.model';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface IProps {
  totalItems: number;
  relatedDocuments: readonly IDocument[];
  getRelatedDocuments: (templateId: string, page?: number, size?: number, sort?: string) => void;
}

export const RelatedDocumentsTab = (props: IProps) => {
  const location = useLocation();
  const history = useHistory();
  const { template: templateId } = useParams<{template: string}>();
  const [pagination, setPagination] = useState(
    overridePaginationStateWithQueryParams(getSortState(location, ITEMS_PER_PAGE), location.search)
  );

  useEffect(() => {
    props.getRelatedDocuments(
      templateId,
      pagination.activePage - 1,
      pagination.itemsPerPage,
      `${pagination.sort},${pagination.order}`
    );
    const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;

    if (location.search !== endURL) {
      history.push(`${location.pathname}${endURL}`);
    }
  }, [pagination.activePage, pagination.order, pagination.sort]);

  useEffect(() => {
    setPagination({
      ...pagination,
      ...overridePaginationStateWithQueryParams(pagination, location.search)
    });
  }, [location.search]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });

  const handlePagination = currentPage =>
    setPagination({
      ...pagination,
      activePage: currentPage,
    });

  return (
    <div>
      <Table responsive striped>
        <thead>
          <tr>
            <th className="hand" onClick={sort('id')}>
              <Translate contentKey="global.field.id">ID</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
          </tr>
        </thead>

        <tbody>
          {props.relatedDocuments.map(document => (
            <tr id={String(document.id)} key={`document-${document.id}`}>
              <td>
                <Button
                  tag={Link}
                  to={`/admin/sds-configuration/document-management/${document.id}`}
                  color="link"
                  size="sm"
                >
                  {document.id}
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {props.totalItems &&
        <div className={props.relatedDocuments?.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount
              page={pagination.activePage}
              total={props.totalItems}
              itemsPerPage={pagination.itemsPerPage}
              i18nEnabled
            />
          </Row>

          <Row className="justify-content-center">
            <JhiPagination
              activePage={pagination.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={pagination.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      }
    </div>
  );
};
