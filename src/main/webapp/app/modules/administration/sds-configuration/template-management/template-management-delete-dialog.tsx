import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { deleteTemplate, getRelatedDocuments, getTemplate } from './template-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const TemplateManagementDeleteDialog = (props: RouteComponentProps<{templateId: string}>) => {
  const dispatch = useAppDispatch();
  const template = useAppSelector(store => store.templateManagement.template);
  const totalRelatedDocuments = useAppSelector(store => store.templateManagement.relatedDocumentTotalItems);

  useEffect(() => {
    dispatch(getTemplate(props.match.params.templateId));
    dispatch(getRelatedDocuments({ templateId: props.match.params.templateId }));
  }, []);

  const handleClose = event => {
    event.stopPropagation();
    props.history.push('/admin/sds-configuration/template-management');
  };

  const confirmDelete = event => {
    dispatch(deleteTemplate(template.id.toString()));
    handleClose(event);
  };

  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>

      <ModalBody>
        <Translate contentKey="templateManagement.delete.question" interpolate={{ name: template.id }}>
          Are you sure you want to delete this Template?
        </Translate>

        {totalRelatedDocuments > 0 &&
          <>
            <br />

            <Translate
              contentKey="templateManagement.delete.warning"
              interpolate={{ total: totalRelatedDocuments }}
            >
              This template will be unlinked from all documents
            </Translate>
          </>
        }

        {template.content?.length > 0 &&
          <>
            <br />

            <Translate
              contentKey="templateManagement.delete.fieldBlockWarning"
              interpolate={{ total: template.content.length }}
            >
              This template will be unlinked from all field blocks
            </Translate>
          </>
        }

      </ModalBody>

      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>

        <Button color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default TemplateManagementDeleteDialog;
