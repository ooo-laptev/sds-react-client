import axios from 'axios';
import { defaultValue, ITemplate, ITemplateLayout } from 'app/shared/model/template.model';
import { IDocument } from 'app/shared/model/document.model';
import { createAsyncThunk, createSlice, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit';
import { IQueryParams, serializeAxiosError } from 'app/shared/reducers/reducer.utils';

const initialState = {
  loading: false,
  errorMessage: null,
  templates: [] as ReadonlyArray<ITemplate>,
  authorities: [] as any[],
  template: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0,
  relatedDocuments: [] as ReadonlyArray<IDocument>,
  relatedDocumentTotalItems: 0
};

export type TemplateManagementState = Readonly<typeof initialState>;

// Actions
const apiUrl = 'api/templates';

export const getTemplates = createAsyncThunk(
  'templateManagement/FETCH_TEMPLATES',
  async ({ page, size, sort }: IQueryParams
) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return axios.get<ITemplate[]>(requestUrl);
});

export const getTemplate = createAsyncThunk('templateManagement/FETCH_TEMPLATE', async (id: string) => {
  return axios.get<ITemplateLayout>(`/api/templateLayout?templateId=${id}`);
});

export const createTemplate = createAsyncThunk(
  'templateManagement/CREATE_TEMPLATE',
  async (template: ITemplateLayout, thunkAPI) => {
    const response = await axios.post(apiUrl, template);
    thunkAPI.dispatch(getTemplates({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

export const updateTemplate = createAsyncThunk(
  'templateManagement/UPDATE_TEMPLATE',
  async (template: ITemplateLayout, thunkAPI) => {
    const response = await axios.put(apiUrl, template);
    thunkAPI.dispatch(getTemplates({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

export const deleteTemplate = createAsyncThunk(
  'templateManagement/DELETE_TEMPLATE',
  async (id: string, thunkAPI) => {
    const response = await axios.delete(`${apiUrl}/${id}`);
    thunkAPI.dispatch(getTemplates({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

interface IGetRelatedDocumentsAction {
  templateId?: string;
  page?: number;
  size?: number;
  sort?: string;
}

export const getRelatedDocuments = createAsyncThunk(
  'templateManagement/FETCH_RELATED_DOCUMENTS',
  async (data: IGetRelatedDocumentsAction) => {
    const sortParams = data.sort ? `&page=${data.page}&size=${data.size}&sort=${data.sort}` : '';
    const requestUrl = `/api/getDocumentsAttachedWithTemplate?templateId=${data.templateId}${sortParams}`;

    return axios.get<IDocument[]>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const addRelatedFieldBlock = createAsyncThunk(
  'templateManagement/ADD_RELATED_FIELD_BLOCK',
  async (data: {templateId: number; fieldBlockId: string}) => {
    return axios.put<ITemplateLayout>(`/api/templateLayout?templateId=${data.templateId}&fieldBlockId=${data.fieldBlockId}`);
  },
  { serializeError: serializeAxiosError }
);

export const deleteRelatedFieldBlock = createAsyncThunk(
  'templateManagement/DELETE_RELATED_FIELD_BLOCK',
  async (data: {templateId: number, fieldBlockId: number, position: number}) => {
    const requestUrl = `/api/templateLayout?templateId=${data.templateId}&fieldBlockId=${data.fieldBlockId}&position=${data.position}`;

    return axios.delete<ITemplateLayout>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const moveRelatedFieldBlock = createAsyncThunk(
  'templateManagement/MOVE_RELATED_FIELD_BLOCK',
  async (data: {templateId: number, fieldBlockId: number, position: number}) => {
    const requestUrl = `/api/templateLayout?templateId=${data.templateId}&fieldBlockId=${data.fieldBlockId}&position=${data.position}`;

    return axios.patch<ITemplateLayout>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const getDefaultCustomFields = createAsyncThunk(
  'templateManagement/GET_DEFAULT_CUSTOM_FIELDS',
  async (templateId: number) => {
    return axios.get(`/api/templateDefaultCustomFields?templateId=${templateId}`);
  },
  { serializeError: serializeAxiosError }
);

export const addDefaultCustomField = createAsyncThunk(
  'templateManagement/ADD_DEFAULT_CUSTOM_FIELD',
  async (data: {templateId: number; customFieldId: string}, thunkAPI) => {
    const requestUrl = `/api/templateDefaultCustomFields?templateId=${data.templateId}&customFieldId=${data.customFieldId}`;

    const response = await axios.put(requestUrl);
    thunkAPI.dispatch(getTemplate(data.templateId.toString()));

    return response;
  },
  { serializeError: serializeAxiosError }
);

export const deleteDefaultCustomField = createAsyncThunk(
  'templateManagement/DELETE_DEFAULT_CUSTOM_FIELD',
  async (data: {templateId: number; customFieldId: number}, thunkAPI) => {
    const requestUrl = `/api/templateDefaultCustomFields?templateId=${data.templateId}&customFieldId=${data.customFieldId}`;

    const response = await axios.delete(requestUrl);
    thunkAPI.dispatch(getTemplate(data.templateId.toString()));

    return response;
  },
  { serializeError: serializeAxiosError }
);

export const moveDefaultCustomField = createAsyncThunk(
  'templateManagement/MOVE_DEFAULT_CUSTOM_FIELD',
  async (data: {templateId: number; customFieldId: number; position: number}, thunkAPI) => {
    const requestUrl = `/api/templateDefaultCustomFields`
      + `?templateId=${data.templateId}&customFieldId=${data.customFieldId}&position=${data.position}`;

    const response = await axios.patch(requestUrl);
    thunkAPI.dispatch(getTemplate(data.templateId.toString()));

    return response;
  },
  { serializeError: serializeAxiosError }
);

export const TemplateManagementSlice = createSlice({
  name: 'templateManagement',
  initialState: initialState as TemplateManagementState,
  reducers: {
    reset() {
      return initialState;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getTemplates.fulfilled, (state, action) => {
        state.loading = false;
        state.templates = action.payload.data;
        state.totalItems = parseInt(action.payload.headers['x-total-count'], 10);
      })
      .addCase(getTemplate.fulfilled, (state, action) => {
        state.loading = false;
        state.template = action.payload.data;
      })
      .addCase(getRelatedDocuments.fulfilled, (state, action) => {
        state.loading = false;
        state.relatedDocuments = action.payload.data;
        state.relatedDocumentTotalItems = parseInt(action.payload.headers['x-total-count'], 10);
      })
      .addCase(deleteTemplate.fulfilled, (state) => {
        state.loading = false;
        state.updateSuccess = true;
        state.template = defaultValue;
      })
      .addMatcher(
        isFulfilled(
          createTemplate,
          updateTemplate,
          addRelatedFieldBlock,
          deleteRelatedFieldBlock,
          moveRelatedFieldBlock,
        ),
        (state, action) => {
          state.loading = false;
          state.updateSuccess = true;
          state.template = action.payload.data;
        }
      )
      .addMatcher(
        isPending(
          getTemplates,
          getTemplate,
          createTemplate,
          updateTemplate,
          deleteTemplate,
          addRelatedFieldBlock,
          deleteRelatedFieldBlock,
          moveRelatedFieldBlock,
        ),
        (state) => {
          state.errorMessage = null;
          state.updateSuccess = false;
          state.loading = true;
        }
      )
      .addMatcher(
        isRejected(
          getTemplates,
          getTemplate,
          createTemplate,
          updateTemplate,
          deleteTemplate,
          addRelatedFieldBlock,
          deleteRelatedFieldBlock,
          moveRelatedFieldBlock,
        ),
        (state, action) => {
          state.loading = false;
          state.updating = false;
          state.updateSuccess = false;
          state.errorMessage = action.payload;
        }
      )
  }
});

export const { reset } = TemplateManagementSlice.actions;

export default TemplateManagementSlice.reducer;
