import React from 'react';
import { ITemplateLayout } from 'app/shared/model/template.model';
import { Translate } from 'react-jhipster';
import { AvField, AvGroup } from 'availity-reactstrap-validation';
import { Label } from 'reactstrap';

interface IProps {
  template: ITemplateLayout;
}

export const GeneralTab = (props: IProps) => {
  const {template} = props;

  return (
    <>
      {!!template.id && (
        <AvGroup>
          <Label for="id">
            <Translate contentKey="global.field.id">ID</Translate>
          </Label>
          <AvField type="text" className="form-control" name="id" required readOnly value={template.id} />
        </AvGroup>
      )}

      <AvGroup>
        <Label for="name">
          <Translate contentKey="templateManagement.name">Name</Translate>
        </Label>
        <AvField
          type="text"
          className="form-control"
          name="name"
          value={template.name}
        />
      </AvGroup>

      <AvGroup>
        <Label for="description">
          <Translate contentKey="templateManagement.description">Description</Translate>
        </Label>
        <AvField
          type="text"
          className="form-control"
          name="description"
          value={template.description}
        />
      </AvGroup>
    </>
  );
};
