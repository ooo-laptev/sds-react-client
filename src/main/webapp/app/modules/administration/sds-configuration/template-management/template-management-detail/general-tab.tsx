import React from 'react';
import { ITemplate } from 'app/shared/model/template.model';
import { Translate } from 'react-jhipster';
import { Row } from 'reactstrap';

interface IProps {
  template: ITemplate;
}

export const GeneralTab = (props: IProps) => {
  const {template} = props;

  return (
    <Row size="md">
      <dl className="jh-entity-details">
        <dt>
          <Translate contentKey="templateManagement.id">ID</Translate>
        </dt>
        <dd>
          <span>{template.id}</span>
        </dd>
        <dt>
          <Translate contentKey="templateManagement.name">Name</Translate>
        </dt>
        <dd>{template.name}</dd>
        <dt>
          <Translate contentKey="templateManagement.description">Description</Translate>
        </dt>
        <dd>{template.description}</dd>
      </dl>
    </Row>
  );
};
