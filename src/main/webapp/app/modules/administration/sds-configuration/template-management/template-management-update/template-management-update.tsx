import React, { useEffect, useMemo, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane } from 'reactstrap';

import { AvForm } from 'availity-reactstrap-validation';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  addDefaultCustomField,
  addRelatedFieldBlock,
  createTemplate,
  deleteDefaultCustomField,
  deleteRelatedFieldBlock,
  getTemplate,
  moveDefaultCustomField,
  moveRelatedFieldBlock,
  reset,
  updateTemplate
} from '../template-management.reducer';
import { GeneralTab } from 'app/modules/administration/sds-configuration/template-management/template-management-update/general-tab';
import { getFieldBlocks } from 'app/modules/administration/sds-configuration/fieldBlock-management/fieldBlock-management.reducer';
import { Dnd } from 'app/shared/layout/dnd/dnd';
import { ICustomField } from 'app/shared/model/customfield.model';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const TemplateManagementUpdate = (props: RouteComponentProps<{ templateId: string }>) => {
  const dispatch = useAppDispatch();
  const [isNew] = useState(!props.match.params || !props.match.params.templateId);
  const [activeTab, setActiveTab] = useState('general');

  const template = useAppSelector(store => store.templateManagement.template);
  const loading = useAppSelector(store => store.templateManagement.loading);
  const updating = useAppSelector(store => store.templateManagement.updating);
  const fieldBlocks = useAppSelector(store => store.fieldBlockManagement.fieldBlocks);

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getTemplate(props.match.params.templateId));
    }

    return () => {
      dispatch(reset());
    };
  }, []);

  const handleChangeTab = tab => () => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const handleClose = () => {
    props.history.push('/admin/sds-configuration/template-management');
  };

  const saveTemplate = (event, values) => {
    if (isNew) {
      dispatch(createTemplate(values));
    } else {
      dispatch(updateTemplate(values));
    }
    handleClose();
  };

  const fullCustomFieldList = useMemo(() => {
    const result: ICustomField[] = [];

    template.content?.forEach(block => {
      block.content?.forEach(field => {
        if (!result.find(item => item.id === field.id)) {
          result.push(field);
        }
      });
    });

    return result;
  }, [template]);

  const handleGetFieldBlocks = () => {
    dispatch(getFieldBlocks({}));
  };

  const handleRelatedFieldBlockAction = (func) => (...args) => {
    dispatch(func({templateId: args[0], fieldBlockId: args[1], position: args[2]}));
  };

  const handleDefaultCustomFieldAction = (func) => (...args) => {
    dispatch(func({templateId: args[0], customFieldId: args[1], position: args[2]}));
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1>
            <Translate contentKey="templateManagement.home.createOrEditLabel">Create or edit a Template</Translate>
          </h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          <Nav tabs>
            <NavItem>
              <NavLink
                className={activeTab === 'general' ? 'active' : ''}
                onClick={handleChangeTab('general')}
              >
                <Translate contentKey="templateManagement.home.generalTab">General</Translate>
              </NavLink>
            </NavItem>

            {!isNew &&
              <>
                <NavItem>
                  <NavLink
                    className={activeTab === 'relatedFieldBlocks' ? 'active' : ''}
                    onClick={handleChangeTab('relatedFieldBlocks')}
                  >
                    <Translate contentKey="templateManagement.home.relatedFieldBlocksTab">Related fieldBlocks</Translate>
                  </NavLink>
                </NavItem>

                <NavItem>
                  <NavLink
                    className={activeTab === 'previewCustomFields' ? 'active' : ''}
                    onClick={handleChangeTab('previewCustomFields')}
                  >
                    <Translate contentKey="templateManagement.home.previewCustomFields">Pre-view CustomFields</Translate>
                  </NavLink>
                </NavItem>
              </>
            }
          </Nav>

          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm onValidSubmit={saveTemplate}>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="general">
                  <GeneralTab template={template} />
                </TabPane>

                <TabPane tabId="relatedFieldBlocks">
                  <Dnd
                    disabled={false}
                    entityHolder={template}
                    fullEntityList={fieldBlocks}
                    getFullEntityList={handleGetFieldBlocks}
                    handleAddEntity={handleRelatedFieldBlockAction(addRelatedFieldBlock)}
                    handleDeleteEntity={handleRelatedFieldBlockAction(deleteRelatedFieldBlock)}
                    handleMoveEntity={handleRelatedFieldBlockAction(moveRelatedFieldBlock)}
                  />
                </TabPane>

                <TabPane tabId="previewCustomFields">
                  <Dnd
                    disabled={false}
                    entityHolder={{id: template.id, content: template.defaultCustomFields || []}}
                    fullEntityList={fullCustomFieldList}
                    handleAddEntity={handleDefaultCustomFieldAction(addDefaultCustomField)}
                    handleDeleteEntity={handleDefaultCustomFieldAction(deleteDefaultCustomField)}
                    handleMoveEntity={handleDefaultCustomFieldAction(moveDefaultCustomField)}
                  />
                </TabPane>
              </TabContent>

              <Button tag={Link} to="/admin/sds-configuration/template-management" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default TemplateManagementUpdate;
