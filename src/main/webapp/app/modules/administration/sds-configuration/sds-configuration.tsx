import React, { useEffect, useState } from 'react';
import { Translate } from 'react-jhipster';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Layout, Menu } from 'antd';
import { BarsOutlined, FolderOpenOutlined, LayoutOutlined } from '@ant-design/icons';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import CustomFieldManagement from 'app/modules/administration/sds-configuration/customfield-management';
import DocumentManagement from 'app/modules/administration/sds-configuration/document-management';
import TemplateManagement from 'app/modules/administration/sds-configuration/template-management';
import FieldBlockManagement from 'app/modules/administration/sds-configuration/fieldBlock-management';

const { SubMenu } = Menu;
const { Content, Sider } = Layout;

const MENU_STYLE = { height: '100%', borderRight: 0 };
const CONTENT_STYLE = { padding: 24, margin: 0, minHeight: 400, };

enum SubMenuItemsEnum {
  'customfield-management' = 'customField',
  'document-management' = 'document',
  'template-management' = 'template',
  'fieldBlock-management' = 'template',
}

const getPageName = (pathname: string): string => {
  return pathname.split('/').find(pathElem => pathElem.includes('management'));
};

export const SDSConfigurationPage = ({ match, history }) => {
  const [selectedKeys, setSelectedKeys] = useState(() => [getPageName(history.location.pathname)]);
  const [openKeys, setOpenKeys] = useState(() => [SubMenuItemsEnum[selectedKeys[0]]]);

  useEffect(() => {
    const pageName = getPageName(history.location.pathname);
    setSelectedKeys([pageName]);
    setOpenKeys([SubMenuItemsEnum[pageName]]);
  }, [history.location.pathname]);

  const handleSubMenuClick = (keys: string[]) => {
    setOpenKeys(keys);
  };

  return (
    <Layout>
      <Sider width={'20%'}>
        <Menu
          mode="inline"
          selectedKeys={selectedKeys}
          openKeys={openKeys}
          style={MENU_STYLE}
          onOpenChange={handleSubMenuClick}
        >
          <SubMenu key="customField" icon={<BarsOutlined />} title="CustomField settings">
            <Menu.Item key="customfield-management">
              <Link to="/admin/sds-configuration/customfield-management">
                <Translate contentKey="global.menu.admin.customfieldManagement">CustomField management</Translate>
              </Link>
            </Menu.Item>
          </SubMenu>

          <SubMenu key="document" icon={<FolderOpenOutlined />} title="Document settings">
            <Menu.Item key="document-management">
              <Link to="/admin/sds-configuration/document-management">
                <Translate contentKey="global.menu.admin.documentManagement">Document management</Translate>
              </Link>
            </Menu.Item>
          </SubMenu>

          <SubMenu key="template" icon={<LayoutOutlined />} title="Templates settings">
            <Menu.Item key="template-management">
              <Link to="/admin/sds-configuration/template-management">
                <Translate contentKey="global.menu.admin.templateManagement">Template management</Translate>
              </Link>
            </Menu.Item>

            <Menu.Item key="fieldBlock-management">
              <Link to="/admin/sds-configuration/fieldBlock-management">
                <Translate contentKey="global.menu.admin.fieldBlockManagement">Field block management</Translate>
              </Link>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>

      <Content style={CONTENT_STYLE}>
        <ErrorBoundaryRoute path={`${match.url}/customfield-management`} component={CustomFieldManagement} />
        <ErrorBoundaryRoute path={`${match.url}/document-management`} component={DocumentManagement} />
        <ErrorBoundaryRoute path={`${match.url}/template-management`} component={TemplateManagement} />
        <ErrorBoundaryRoute path={`${match.url}/fieldBlock-management`} component={FieldBlockManagement} />
      </Content>
    </Layout>
  );
};

export default SDSConfigurationPage;
