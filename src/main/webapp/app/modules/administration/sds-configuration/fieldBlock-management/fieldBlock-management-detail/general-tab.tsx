import React from 'react';
import { IFieldBlockLayout } from 'app/shared/model/fieldBlock.model';
import { Translate } from 'react-jhipster';
import { Row } from 'reactstrap';

interface IProps {
  fieldBlock: IFieldBlockLayout;
}

export const GeneralTab = (props: IProps) => {
  const { fieldBlock } = props;

  return (
    <Row size="md">
      <dl className="jh-entity-details">
        <dt>
          <Translate contentKey="fieldBlockManagement.id">ID</Translate>
        </dt>
        <dd>
          <span>{fieldBlock.id}</span>
        </dd>
        <dt>
          <Translate contentKey="fieldBlockManagement.name">Name</Translate>
        </dt>
        <dd>{fieldBlock.name}</dd>
        <dt>
          <Translate contentKey="fieldBlockManagement.description">Description</Translate>
        </dt>
        <dd>{fieldBlock.description}</dd>
      </dl>
    </Row>
  );
};
