import React, { useEffect, useState } from 'react';
import { getSortState, JhiItemCount, JhiPagination, Translate } from 'react-jhipster';
import { Row, Table, Button} from 'reactstrap';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { useLocation, useHistory, Link, useParams } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ITemplate } from 'app/shared/model/template.model';

interface IProps {
  totalItems: number;
  relatedTemplates: readonly ITemplate[];
  getRelatedTemplates: (fieldBlockId: string, page?: number, size?: number, sort?: string) => void;
}

export const RelatedTemplatesTab = (props: IProps) => {
  const location = useLocation();
  const history = useHistory();
  const { fieldBlock: fieldBlockId } = useParams<{fieldBlock: string}>();
  const [pagination, setPagination] = useState(
    overridePaginationStateWithQueryParams(getSortState(location, ITEMS_PER_PAGE), location.search)
  );

  useEffect(() => {
    props.getRelatedTemplates(
      fieldBlockId,
      pagination.activePage - 1,
      pagination.itemsPerPage,
      `${pagination.sort},${pagination.order}`
    );
    const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;

    if (location.search !== endURL) {
      history.push(`${location.pathname}${endURL}`);
    }
  }, [pagination.activePage, pagination.order, pagination.sort]);

  useEffect(() => {
    setPagination({
      ...pagination,
      ...overridePaginationStateWithQueryParams(pagination, location.search)
    });
  }, [location.search]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });

  const handlePagination = currentPage =>
    setPagination({
      ...pagination,
      activePage: currentPage,
    });

  return (
    <div>
      <Table responsive striped>
        <thead>
          <tr>
            <th className="hand" onClick={sort('name')}>
              <Translate contentKey="global.field.name">Name</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>

            <th className="hand" onClick={sort('description')}>
              <Translate contentKey="global.field.description">Description</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
          </tr>
        </thead>

        <tbody>
          {props.relatedTemplates.map(template => (
            <tr id={String(template.id)} key={`template-${template.id}`}>
              <td>
                <Button
                  tag={Link}
                  to={`/admin/sds-configuration/template-management/${template.id}`}
                  color="link"
                  size="sm"
                >
                  {template.name}
                </Button>
              </td>

              <td>{template.description}</td>
            </tr>
          ))}
        </tbody>
      </Table>

      {props.totalItems > 0 &&
        <div className={props.relatedTemplates?.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount
              page={pagination.activePage}
              total={props.totalItems}
              itemsPerPage={pagination.itemsPerPage}
              i18nEnabled
            />
          </Row>

          <Row className="justify-content-center">
            <JhiPagination
              activePage={pagination.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={pagination.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      }
    </div>
  );
};
