import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Table } from 'reactstrap';
import { getSortState, JhiItemCount, JhiPagination, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { getFieldBlocks } from './fieldBlock-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FieldBlockManagement = (props: RouteComponentProps) => {
  const dispatch = useAppDispatch();
  const [pagination, setPagination] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE), props.location.search)
  );

  useEffect(() => {
    dispatch(getFieldBlocks({
      page: pagination.activePage - 1, size: pagination.itemsPerPage, sort: `${pagination.sort},${pagination.order}`
    }));
    const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  }, [pagination.activePage, pagination.order, pagination.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get('sort');
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPagination({
        ...pagination,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === 'asc' ? 'desc' : 'asc',
      sort: p,
    });

  const handlePagination = currentPage =>
    setPagination({
      ...pagination,
      activePage: currentPage,
    });

  const fieldBlocks = useAppSelector(store => store.fieldBlockManagement.fieldBlocks);
  const totalItems = useAppSelector(store => store.fieldBlockManagement.totalItems);

  const { match } = props;

  return (
    <div>
      <h2 id="fieldBlock-management-page-heading">
        <Translate contentKey="fieldBlockManagement.home.title">FieldBlocks</Translate>
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity">
          <FontAwesomeIcon icon="plus" /> <Translate contentKey="fieldBlockManagement.home.createLabel">Create a new
          fieldBlock</Translate>
        </Link>
      </h2>
      <Table responsive striped>
        <thead>
          <tr>
            <th className="hand" onClick={sort('id')}>
              <Translate contentKey="global.field.id">ID</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
            <th className="hand" onClick={sort('name')}>
              <Translate contentKey="fieldBlockManagement.name">Name</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
            <th className="hand" onClick={sort('description')}>
              <Translate contentKey="fieldBlockManagement.description">Description</Translate>
              <FontAwesomeIcon icon="sort" />
            </th>
          </tr>
        </thead>
        <tbody>
          {fieldBlocks.map((fieldBlock, i) => (
            <tr id={String(fieldBlock.id)} key={`fieldBlock-${i}`}>
              <td>
                <Button tag={Link} to={`${match.url}/${fieldBlock.id}`} color="link" size="sm">
                  {fieldBlock.id}
                </Button>
              </td>
              <td>{fieldBlock.name}</td>
              <td>{fieldBlock.description}</td>
              <td className="text-right">
                <div className="btn-group flex-btn-group-container">
                  <Button tag={Link} to={`${match.url}/${fieldBlock.id}`} color="info" size="sm">
                    <FontAwesomeIcon icon="eye" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.view">View</Translate>
                    </span>
                  </Button>
                  <Button tag={Link} to={`${match.url}/${fieldBlock.id}/edit`} color="primary" size="sm">
                    <FontAwesomeIcon icon="pencil-alt" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.edit">Edit</Translate>
                    </span>
                  </Button>
                  <Button
                    tag={Link}
                    to={`${match.url}/${fieldBlock.id}/delete`}
                    color="danger"
                    size="sm"
                  >
                    <FontAwesomeIcon icon="trash" />{' '}
                    <span className="d-none d-md-inline">
                      <Translate contentKey="entity.action.delete">Delete</Translate>
                    </span>
                  </Button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      {totalItems > 0 && (
        <div className={fieldBlocks && fieldBlocks.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={pagination.activePage} total={totalItems} itemsPerPage={pagination.itemsPerPage} i18nEnabled />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={pagination.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={pagination.itemsPerPage}
              totalItems={totalItems}
            />
          </Row>
        </div>
      )}
    </div>
  );
};

export default FieldBlockManagement;
