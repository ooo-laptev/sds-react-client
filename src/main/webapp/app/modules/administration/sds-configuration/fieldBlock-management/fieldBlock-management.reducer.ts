import axios from 'axios';
import { defaultValue, IFieldBlock, IFieldBlockLayout } from 'app/shared/model/fieldBlock.model';
import { ITemplate } from 'app/shared/model/template.model';
import { createAsyncThunk, createSlice, isFulfilled, isPending, isRejected } from '@reduxjs/toolkit';
import { IQueryParams, serializeAxiosError } from 'app/shared/reducers/reducer.utils';

const initialState = {
  loading: false,
  errorMessage: null,
  fieldBlocks: [] as ReadonlyArray<IFieldBlock>,
  authorities: [] as any[],
  fieldBlock: defaultValue,
  updating: false,
  updateSuccess: false,
  totalItems: 0,
  relatedTemplates: [] as Readonly<ITemplate[]>,
  relatedTemplateTotalItems: 0
};

export type FieldBlockManagementState = Readonly<typeof initialState>;

// Actions

const apiUrl = 'api/fieldBlocks';

export const getFieldBlocks = createAsyncThunk(
  'fieldBlockManagement/FETCH_FIELDBLOCKS',
  async ({ page, size, sort }: IQueryParams
) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return axios.get<IFieldBlock[]>(requestUrl);
});

export const getFieldBlock = createAsyncThunk('fieldBlockManagement/FETCH_FIELDBLOCK', async (id: string) => {
  return axios.get<IFieldBlockLayout>(`/api/fieldBlockLayout?fieldBlockId=${id}`);
});

export const createFieldBlock = createAsyncThunk(
  'fieldBlockManagement/CREATE_FIELDBLOCK',
  async (fieldBlock: IFieldBlockLayout, thunkAPI) => {
    const response = await axios.post(apiUrl, fieldBlock);
    thunkAPI.dispatch(getFieldBlocks({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

export const updateFieldBlock = createAsyncThunk(
  'fieldBlockManagement/UPDATE_FIELDBLOCK',
  async (fieldBlock: IFieldBlockLayout, thunkAPI) => {
    const response = await axios.put(apiUrl, fieldBlock);
    thunkAPI.dispatch(getFieldBlocks({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

export const deleteFieldBlock = createAsyncThunk(
  'fieldBlockManagement/DELETE_FIELDBLOCK',
  async (id: string, thunkAPI) => {
    const response = await axios.delete(`${apiUrl}/${id}`);
    thunkAPI.dispatch(getFieldBlocks({}));
    return response;
  },
  { serializeError: serializeAxiosError }
);

interface IRelatedTemplatesActionData {
  fieldBlockId?: string;
  page?: number;
  size?: number;
  sort?: string;
}

export const getRelatedTemplates = createAsyncThunk(
  'fieldBlockManagement/FETCH_RELATED_TEMPLATES',
  async (data: IRelatedTemplatesActionData) => {
    const sortParams = data.sort ? `&page=${data.page}&size=${data.size}&sort=${data.sort}` : '';
    const requestUrl = `/api/getRelatedTemplates?fieldBlockId=${data.fieldBlockId}${sortParams}`;

    return axios.get<ITemplate[]>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const addRelatedField = createAsyncThunk(
  'fieldBlockManagement/ADD_RELATED_FIELD',
  async (data: {fieldBlockId: number; customFieldId: string;}) => {
    return axios.put<IFieldBlockLayout>(`/api/fieldBlockLayout?fieldBlockId=${data.fieldBlockId}&customFieldId=${data.customFieldId}`);
  },
  { serializeError: serializeAxiosError }
);

interface IRelatedFieldsManipulationActionData {
  fieldBlockId: number;
  customFieldId: number;
  position: number
}

export const deleteRelatedField = createAsyncThunk(
  'fieldBlockManagement/DELETE_RELATED_FIELD',
  async ({fieldBlockId, customFieldId, position}: IRelatedFieldsManipulationActionData) => {
    const requestUrl = `/api/fieldBlockLayout?fieldBlockId=${fieldBlockId}&customFieldId=${customFieldId}&position=${position}`;
    return axios.delete<IFieldBlockLayout>(requestUrl)
  },
  { serializeError: serializeAxiosError }
);

export const moveRelatedField = createAsyncThunk(
  'fieldBlockManagement/MOVE_RELATED_FIELD',
  async ({fieldBlockId, customFieldId, position}: IRelatedFieldsManipulationActionData) => {
    const requestUrl = `/api/fieldBlockLayout?fieldBlockId=${fieldBlockId}&customFieldId=${customFieldId}&position=${position}`;
    return axios.patch<IFieldBlockLayout>(requestUrl)
  },
  { serializeError: serializeAxiosError }
);

export const FieldBlockManagementSlice = createSlice({
  name: 'fieldBlockManagement',
  initialState: initialState as FieldBlockManagementState,
  reducers: {
    reset() {
      return initialState;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(getFieldBlocks.fulfilled, (state, action) => {
        state.loading = false;
        state.fieldBlocks = action.payload.data;
        state.totalItems = parseInt(action.payload.headers['x-total-count'], 10);
      })
      .addCase(getFieldBlock.fulfilled, (state, action) => {
        state.loading = false;
        state.fieldBlock = action.payload.data;
      })
      .addCase(deleteFieldBlock.fulfilled, (state) => {
        state.loading = false;
        state.updateSuccess = true;
        state.fieldBlock = defaultValue;
      })
      .addCase(getRelatedTemplates.fulfilled, (state, action) => {
        state.loading = false;
        state.relatedTemplates = action.payload.data;
        state.relatedTemplateTotalItems = parseInt(action.payload.headers['x-total-count'], 10);
      })
      .addMatcher(
        isFulfilled(createFieldBlock, updateFieldBlock, addRelatedField, deleteRelatedField, moveRelatedField), (state, action) => {
          state.loading = false;
          state.updating = false;
          state.updateSuccess = true;
          state.fieldBlock = action.payload.data;
      })
      .addMatcher(
        isPending(
          getFieldBlocks,
          getFieldBlock,
          createFieldBlock,
          updateFieldBlock,
          deleteFieldBlock,
          getRelatedTemplates,
          addRelatedField,
          deleteRelatedField,
          moveRelatedField,
        ),
        (state) => {
          state.errorMessage = null;
          state.updateSuccess = false;
          state.loading = true;
        })
      .addMatcher(
        isRejected(
          getFieldBlocks,
          getFieldBlock,
          createFieldBlock,
          updateFieldBlock,
          deleteFieldBlock,
          getRelatedTemplates,
          addRelatedField,
          deleteRelatedField,
          moveRelatedField,
        ),
        (state, action) => {
          state.loading = false;
          state.updating = false;
          state.updateSuccess = false;
          state.errorMessage = action.error.message;
        });
  }
});

export const { reset } = FieldBlockManagementSlice.actions;

export default FieldBlockManagementSlice.reducer;
