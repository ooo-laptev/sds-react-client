import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { deleteFieldBlock, getFieldBlock, getRelatedTemplates } from './fieldBlock-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FieldBlockManagementDeleteDialog = (props: RouteComponentProps<{fieldBlock: string}>) => {
  const dispatch = useAppDispatch();
  const fieldBlock = useAppSelector(store => store.fieldBlockManagement.fieldBlock);
  const totalRelatedTemplates = useAppSelector(store => store.fieldBlockManagement.relatedTemplateTotalItems);

  useEffect(() => {
    dispatch(getFieldBlock(props.match.params.fieldBlock));
    dispatch(getRelatedTemplates({fieldBlockId: props.match.params.fieldBlock}));
  }, []);

  const handleClose = event => {
    event.stopPropagation();
    props.history.push('/admin/sds-configuration/fieldBlock-management');
  };

  const confirmDelete = event => {
    dispatch(deleteFieldBlock(fieldBlock.id.toString()));
    handleClose(event);
  };

  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>
        <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
      </ModalHeader>

      <ModalBody>
        <Translate contentKey="fieldBlockManagement.delete.question" interpolate={{ name: fieldBlock.id }}>
          Are you sure you want to delete this FieldBlock?
        </Translate>

        {totalRelatedTemplates > 0 &&
          <>
            <br />

            <Translate
              contentKey="fieldBlockManagement.delete.templateWarning"
              interpolate={{ total: totalRelatedTemplates }}
            >
              This fieldBlock will be unlinked from all templates
            </Translate>
          </>
        }

        {fieldBlock.content?.length > 0 &&
          <>
            <br />

            <Translate
              contentKey="fieldBlockManagement.delete.fieldWarning"
              interpolate={{ total: fieldBlock.content.length }}
            >
              This fieldBlock will be unlinked from all custom fields
            </Translate>
          </>
        }
      </ModalBody>

      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp;
          <Translate contentKey="entity.action.cancel">Cancel</Translate>
        </Button>
        <Button color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp;
          <Translate contentKey="entity.action.delete">Delete</Translate>
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default FieldBlockManagementDeleteDialog;
