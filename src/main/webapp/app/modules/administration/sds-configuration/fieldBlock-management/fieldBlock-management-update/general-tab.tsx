import React from 'react';
import { IFieldBlockLayout } from 'app/shared/model/fieldBlock.model';
import { Translate } from 'react-jhipster';
import { AvField, AvGroup } from 'availity-reactstrap-validation';
import { Label } from 'reactstrap';

interface IProps {
  fieldBlock: IFieldBlockLayout;
}

export const GeneralTab = (props: IProps) => {
  const { fieldBlock } = props;

  return (
    <>
      {!!fieldBlock.id && (
        <AvGroup>
          <Label for="id">
            <Translate contentKey="global.field.id">ID</Translate>
          </Label>
          <AvField type="text" className="form-control" name="id" required readOnly value={fieldBlock.id} />
        </AvGroup>
      )}
      <AvGroup>
        <Label for="name">
          <Translate contentKey="fieldBlockManagement.name">Name</Translate>
        </Label>
        <AvField
          type="text"
          className="form-control"
          name="name"
          value={fieldBlock.name}
        />
      </AvGroup>
      <AvGroup>
        <Label for="description">
          <Translate contentKey="fieldBlockManagement.description">Description</Translate>
        </Label>
        <AvField
          type="text"
          className="form-control"
          name="description"
          value={fieldBlock.description}
        />
      </AvGroup>
    </>
  );
};
