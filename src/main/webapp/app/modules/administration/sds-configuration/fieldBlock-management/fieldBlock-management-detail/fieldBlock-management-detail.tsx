import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  addRelatedField,
  deleteRelatedField,
  getFieldBlock,
  getRelatedTemplates,
  moveRelatedField
} from '../fieldBlock-management.reducer';
import { getAllCustomFields } from 'app/modules/administration/sds-configuration/customfield-management/customfield-management.reducer';
import { Dnd } from 'app/shared/layout/dnd/dnd';
import { GeneralTab } from 'app/modules/administration/sds-configuration/fieldBlock-management/fieldBlock-management-detail/general-tab';
import {
  RelatedTemplatesTab
} from 'app/modules/administration/sds-configuration/fieldBlock-management/fieldBlock-management-detail/related-templates-tab';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FieldBlockManagementDetail = (props: RouteComponentProps<{fieldBlock: string}>) => {
  const dispatch = useAppDispatch();
  const [activeTab, setActiveTab] = useState('general');
  const fieldBlock = useAppSelector(store => store.fieldBlockManagement.fieldBlock);
  const relatedTemplates = useAppSelector(store => store.fieldBlockManagement.relatedTemplates);
  const totalItems = useAppSelector(store => store.fieldBlockManagement.relatedTemplateTotalItems);
  const customFields = useAppSelector(store => store.customfieldManagement.customfields);

  useEffect(() => {
    dispatch(getFieldBlock(props.match.params.fieldBlock));
  }, []);

  const toggle = tab => () => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const handleGetFullEntityList = () => {
    dispatch(getAllCustomFields());
  };

  const handleRelatedFieldAction = (func) => (...args) => {
    dispatch(func({fieldBlockId: args[0], customFieldId: args[1], position: args[2]}));
  };

  const handleGetRelatedTemplates = (fieldBlockId: string, page?: number, size?: number, sort?: string) => {
    dispatch(getRelatedTemplates({fieldBlockId, page, size, sort}));
  };

  return (
    <div>
      <h2>
        <Translate contentKey="fieldBlockManagement.detail.title">FieldBlock</Translate> [<b>{fieldBlock.id}</b>]
      </h2>

      <Nav tabs>
        <NavItem>
          <NavLink
            className={activeTab === 'general' ? 'active' : ''}
            onClick={toggle('general')}
          >
            <Translate contentKey="fieldBlockManagement.detail.generalTab">General</Translate>
          </NavLink>
        </NavItem>

        <NavItem>
          <NavLink
            className={activeTab === 'relatedTemplates' ? 'active' : ''}
            onClick={toggle('relatedTemplates')}
          >
            <Translate contentKey="fieldBlockManagement.detail.relatedTemplatesTab">Related templates</Translate>
          </NavLink>
        </NavItem>

        <NavItem>
          <NavLink
            className={activeTab === 'relatedFields' ? 'active' : ''}
            onClick={toggle('relatedFields')}
          >
            <Translate contentKey="fieldBlockManagement.detail.relatedFieldsTab">Related fields</Translate>
          </NavLink>
        </NavItem>
      </Nav>

      <TabContent activeTab={activeTab}>
        <TabPane tabId="general">
          <GeneralTab fieldBlock={fieldBlock} />
        </TabPane>

        <TabPane tabId="relatedTemplates">
          <RelatedTemplatesTab
            totalItems={totalItems}
            relatedTemplates={relatedTemplates}
            getRelatedTemplates={handleGetRelatedTemplates}
          />
        </TabPane>

        <TabPane tabId="relatedFields">
          <Dnd
            disabled
            entityHolder={fieldBlock}
            fullEntityList={customFields}
            getFullEntityList={handleGetFullEntityList}
            handleAddEntity={handleRelatedFieldAction(addRelatedField)}
            handleDeleteEntity={handleRelatedFieldAction(deleteRelatedField)}
            handleMoveEntity={handleRelatedFieldAction(moveRelatedField)}
          />
        </TabPane>
      </TabContent>

      <Button tag={Link} to="/admin/sds-configuration/fieldBlock-management" replace color="info">
        <FontAwesomeIcon icon="arrow-left" />{' '}
        <span className="d-none d-md-inline">
          <Translate contentKey="entity.action.back">Back</Translate>
        </span>
      </Button>
    </div>
  );
};

export default FieldBlockManagementDetail;
