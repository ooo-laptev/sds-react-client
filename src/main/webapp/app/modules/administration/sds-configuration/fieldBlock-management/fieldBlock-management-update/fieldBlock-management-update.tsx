import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane } from 'reactstrap';
import { AvForm } from 'availity-reactstrap-validation';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  addRelatedField,
  createFieldBlock,
  deleteRelatedField,
  getFieldBlock,
  moveRelatedField,
  reset,
  updateFieldBlock
} from '../fieldBlock-management.reducer';
import { GeneralTab } from 'app/modules/administration/sds-configuration/fieldBlock-management/fieldBlock-management-update/general-tab';
import { Dnd } from 'app/shared/layout/dnd/dnd';
import { getAllCustomFields } from 'app/modules/administration/sds-configuration/customfield-management/customfield-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const FieldBlockManagementUpdate = (props: RouteComponentProps<{fieldBlock: string}>) => {
  const dispatch = useAppDispatch();
  const [isNew] = useState(!props.match.params || !props.match.params.fieldBlock);
  const [activeTab, setActiveTab] = useState('general');

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getFieldBlock(props.match.params.fieldBlock));
    }
    return () => {
      dispatch(reset());
    };
  }, []);

  const handleChangeTab = tab => () => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const handleClose = () => {
    props.history.push('/admin/sds-configuration/fieldBlock-management');
  };

  const saveFieldBlock = (event, values) => {
    if (isNew) {
      dispatch(createFieldBlock(values));
    } else {
      dispatch(updateFieldBlock(values));
    }
    handleClose();
  };

  const handleGetFullEntityList = () => {
    dispatch(getAllCustomFields());
  };

  const handleRelatedFieldAction = (func) => (...args) => {
    dispatch(func({fieldBlockId: args[0], customFieldId: args[1], position: args[2]}));
  };

  const fieldBlock = useAppSelector(store => store.fieldBlockManagement.fieldBlock);
  const loading = useAppSelector(store => store.fieldBlockManagement.loading);
  const updating = useAppSelector(store => store.fieldBlockManagement.updating);
  const customFields = useAppSelector(store => store.customfieldManagement.customfields);

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h1>
            <Translate contentKey="fieldBlockManagement.home.createOrEditLabel">Create or edit a
            FieldBlock</Translate>
          </h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          <Nav tabs>
            <NavItem>
              <NavLink
                className={activeTab === 'general' ? 'active' : ''}
                onClick={handleChangeTab('general')}
              >
                <Translate contentKey="fieldBlockManagement.home.generalTab">General</Translate>
              </NavLink>
            </NavItem>

            {!isNew &&
            <NavItem>
              <NavLink
                className={activeTab === 'relatedFields' ? 'active' : ''}
                onClick={handleChangeTab('relatedFields')}
              >
                <Translate contentKey="fieldBlockManagement.home.relatedFieldsTab">Related fields</Translate>
              </NavLink>
            </NavItem>
            }
          </Nav>

          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm onValidSubmit={saveFieldBlock}>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="general">
                  <GeneralTab fieldBlock={fieldBlock}/>
                </TabPane>

                <TabPane tabId="relatedFields">
                  <Dnd
                    disabled={false}
                    entityHolder={fieldBlock}
                    fullEntityList={customFields}
                    getFullEntityList={handleGetFullEntityList}
                    handleAddEntity={handleRelatedFieldAction(addRelatedField)}
                    handleDeleteEntity={handleRelatedFieldAction(deleteRelatedField)}
                    handleMoveEntity={handleRelatedFieldAction(moveRelatedField)}
                  />
                </TabPane>
              </TabContent>

              <Button tag={Link} to="/admin/sds-configuration/fieldBlock-management" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default FieldBlockManagementUpdate;
