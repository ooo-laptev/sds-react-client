import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import FieldBlockManagement from './fieldBlock-management';
import FieldBlockManagementDetail from './fieldBlock-management-detail/fieldBlock-management-detail';
import FieldBlockManagementUpdate from './fieldBlock-management-update/fieldBlock-management-update';
import FieldBlockManagementDeleteDialog from './fieldBlock-management-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FieldBlockManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:fieldBlock/edit`} component={FieldBlockManagementUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:fieldBlock`} component={FieldBlockManagementDetail} />
      <ErrorBoundaryRoute path={match.url} component={FieldBlockManagement} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:fieldBlock/delete`} component={FieldBlockManagementDeleteDialog} />
  </>
);

export default Routes;
