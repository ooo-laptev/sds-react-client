import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { InputNumber as InputNumeric } from 'antd';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  maximumFractionDigits?: number;
  onChange: (id: number, value?: string) => void;
  inlineLabel?: boolean;
}

export const InputNumber = (props: IProps) => {
  const { field, disabled, maximumFractionDigits = 0, inlineLabel} = props;

  const handleChange = (value) => {
    props.onChange(field.id, value);
  };

  return (
    <div className={`input-wrapper input-wrapper--number ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      <InputNumeric
        value={field.value}
        onChange={handleChange}
        disabled={disabled}
        decimalSeparator=','
        formatter={value =>
          new Intl.NumberFormat('ru-RU', { maximumFractionDigits }).format(value)
        }
      />
    </div>
  );
};
