import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { TimePicker } from 'antd';
import moment from 'moment';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string) => void;
  inlineLabel?: boolean;
}

export const InputTime = (props: IProps) => {
  const { field, disabled, inlineLabel} = props;

  const handleChange = (time, timeToString) => {
    props.onChange(field.id, timeToString);
  };

  return (
    <div className={`input-wrapper ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      <TimePicker
        value={field.value ? moment(field.value, 'HH:mm:ss') : undefined}
        onChange={handleChange}
        disabled={disabled}
        size='small'
      />
    </div>
  );
};
