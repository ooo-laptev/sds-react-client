import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { DatePicker, TimePicker } from 'antd';
import moment from 'moment';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string) => void;
  inlineLabel?: boolean;
}

export const InputDateTime = (props: IProps) => {
  const { field, disabled} = props;

  const handleChange = (part) => (momentValue) => {
    const currentValue = field.value ? moment(field.value).toISOString() : undefined;
    let value = momentValue.toISOString();

    if (currentValue) {
      if (part === 'date') {
        value = value.split('T')[0] + 'T' + currentValue.split('T')[1];
      } else {
        value = currentValue.split('T')[0] + 'T' + value.split('T')[1];
      }
    }

    props.onChange(field.id, value);
  };

  return (
    <div className='input-wrapper input-wrapper--date-time'>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      <div>
        <DatePicker
          value={field.value ? moment(field.value) : undefined}
          onChange={handleChange('date')}
          disabled={disabled}
          size='small'
          format='DD.MM.YYYY'
        />

        <TimePicker
          value={field.value ? moment(field.value) : undefined}
          onChange={handleChange('time')}
          disabled={disabled}
          size='small'
        />
      </div>
    </div>
  );
};
