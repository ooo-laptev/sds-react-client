import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { Input } from 'antd';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string) => void;
  inlineLabel?: boolean;
}

export const InputText = (props: IProps) => {
  const { field, disabled, inlineLabel} = props;

  const handleChange = (e) => {
    props.onChange(field.id, e.target.value);
  };

  return (
    <div className={`input-wrapper ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      <Input type='text' value={field.value} onChange={handleChange} disabled={disabled} />
    </div>
  );
};
