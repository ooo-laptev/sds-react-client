import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import MaskedInput from 'antd-mask-input'
import { translate } from 'react-jhipster';
import { Tooltip } from 'antd';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string, error?: string) => void;
  inlineLabel?: boolean;
}

export const InputPhone = (props: IProps) => {
  const { field, disabled, inlineLabel} = props;

  const removeMask = (value: string) => value.replace(/[^0-9+]/g, '');

  const handleChange = (e) => {
    props.onChange(field.id, removeMask(e.target.value));
  };

  const handleBlur = (e) => {
    const value = removeMask(e.target.value);
    if (value?.length !== 12) {
      props.onChange(field.id, value, translate('global.messages.validate.phone.invalid'));
    }
  };

  const getMaskedValue = () => {
    const matchArray = field.value?.match(/([0-9+]{2})(\d{3})(\d{3})(\d{2})(\d{2})/);

    return matchArray
      ? `${matchArray[1]} (${matchArray[2]}) ${matchArray[3]}-${matchArray[4]}-${matchArray[5]}`
      : field.value;
  };

  return (
    <div className={`input-wrapper ${field.error ? 'input-wrapper--error' : ''} ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      {disabled ?
        <a href={`tel:${field.value}`} style={{alignSelf: 'flex-start'}}>
          {getMaskedValue()}
        </a>
        :
        <Tooltip title={field.error}>
          <MaskedInput
            type='tel'
            mask='+7 (111) 111-11-11'
            value={field.value}
            onChange={handleChange}
            onBlur={handleBlur}
            disabled={disabled}
          />
        </Tooltip>
      }
    </div>
  );
};
