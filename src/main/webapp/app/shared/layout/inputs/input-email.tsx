import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { Input, Tooltip } from 'antd';
import { translate } from 'react-jhipster';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string, error?: string) => void;
  inlineLabel?: boolean;
}

const EMAIL_REGEXP =
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/igm;

export const InputEmail = (props: IProps) => {
  const { field, disabled, inlineLabel} = props;

  const handleChange = (e) => {
    props.onChange(field.id, e.target.value);
  };

  const handleBlur = (e) => {
    if (!EMAIL_REGEXP.test(e.target.value)) {
      props.onChange(field.id, e.target.value, translate('global.messages.validate.email.invalid'));
    }
  };

  return (
    <div className={`input-wrapper ${field.error ? 'input-wrapper--error' : ''} ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      {disabled ?
        <a href={`mailto:${field.value}`} style={{alignSelf: 'flex-start'}}>{field.value}</a>
        :
        <Tooltip title={field.error}>
          <Input
            type='email'
            value={field.value}
            onChange={handleChange}
            onBlur={handleBlur}
            disabled={disabled}
          />
        </Tooltip>
      }
    </div>
  );
};
