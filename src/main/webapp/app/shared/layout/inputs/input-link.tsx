import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { Input } from 'antd';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string) => void;
  inlineLabel?: boolean;
}

export const InputLink = (props: IProps) => {
  const { field, disabled, inlineLabel} = props;

  const handleChange = (e) => {
    props.onChange(field.id, e.target.value);
  };

  return (
    <div className={`input-wrapper ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      {disabled ?
        <a
          href={field.value}
          style={{alignSelf: 'flex-start'}}
          rel='noopener noreferrer'
          target='_blank'
        >
          {field.value}
        </a>
        :
        <Input
          value={field.value}
          onChange={handleChange}
          disabled={disabled}
        />
      }
    </div>
  );
};
