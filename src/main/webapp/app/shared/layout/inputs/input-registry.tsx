import React from 'react';
import './input.scss';

import { ICustomField } from 'app/shared/model/customfield.model';
import { InputText } from 'app/shared/layout/inputs/input-text';
import { InputNumber } from 'app/shared/layout/inputs/input-number';
import { InputCheckbox } from 'app/shared/layout/inputs/input-checkbox';
import { InputTextarea } from 'app/shared/layout/inputs/input-textarea';
import { InputDate } from 'app/shared/layout/inputs/input-date';
import { InputPhone } from 'app/shared/layout/inputs/input-phone';
import { InputEmail } from 'app/shared/layout/inputs/input-email';
import { InputLink } from 'app/shared/layout/inputs/input-link';
import { InputTime } from 'app/shared/layout/inputs/input-time';
import { InputDateTime } from 'app/shared/layout/inputs/input-date-time';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange?: (id: number, value?: any, error?: string) => void;
  inlineLabel?: boolean;
}

export enum InputTypeEnum {
  TEXT = 'StringCustomField',
  INTEGER = 'IntegerCustomField',
  DOUBLE = 'DoubleCustomField',
  CHECKBOX = 'CheckboxCustomField',
  TEXTAREA = 'TextboxCustomField',
  DATE = 'DateCustomField',
  TIME = 'TimeCustomField',
  DATETIME = 'DateTimeCustomField',
  PHONE = 'TelephoneNumberCustomField',
  EMAIL = 'EmailCustomField',
  LINK = 'HyperLinkCustomField',
}

export const getCustomFieldKeyType = (type: string) => {
  const fieldTypeSplitArray = type.split('.');
  return fieldTypeSplitArray[fieldTypeSplitArray.length - 1];
};

export const InputRegistry = (props: IProps) => {
  const {field, onChange, disabled, inlineLabel} = props;

  const getInput = () => {
    const fieldType = getCustomFieldKeyType(field.type);

    switch (fieldType) {
      case InputTypeEnum.INTEGER:
        return (
          <InputNumber field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.DOUBLE:
        return (
          <InputNumber field={field} disabled={disabled} onChange={onChange} maximumFractionDigits={8} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.CHECKBOX:
        return (
          <InputCheckbox field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.TEXTAREA:
        return (
          <InputTextarea field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.DATE:
        return (
          <InputDate field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.TIME:
        return (
          <InputTime field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.DATETIME:
        return (
          <InputDateTime field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.PHONE:
        return (
          <InputPhone field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.EMAIL:
        return (
          <InputEmail field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      case InputTypeEnum.LINK:
        return (
          <InputLink field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );

      default:
        return (
          <InputText field={field} disabled={disabled} onChange={onChange} inlineLabel={inlineLabel} />
        );
    }
  };

  return getInput();
};
