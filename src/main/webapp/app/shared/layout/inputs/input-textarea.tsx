import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { Input } from 'antd';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string) => void;
  inlineLabel?: boolean;
}

export const InputTextarea = (props: IProps) => {
  const { field, disabled, inlineLabel} = props;
  const { TextArea } = Input;

  const handleChange = (e) => {
    props.onChange(field.id, e.target.value);
  };

  return (
    <div className={`input-wrapper ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      <TextArea value={field.value} onChange={handleChange} rows={4} disabled={disabled} />
    </div>
  );
};
