import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { Checkbox } from 'antd';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: any) => void;
  inlineLabel?: boolean;
}

export const InputCheckbox = (props: IProps) => {
  const { field, disabled} = props;

  const handleChange = (e) => {
    props.onChange(field.id, e.target.checked);
  };

  return (
    <div className='input-wrapper input-wrapper--checkbox'>
      <Checkbox
        checked={field.value}
        onChange={handleChange}
        disabled={disabled}
      >
        {field.name}
      </Checkbox>
    </div>
  );
};
