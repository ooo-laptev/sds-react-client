import React from 'react';
import { ICustomField } from 'app/shared/model/customfield.model';
import { DatePicker } from 'antd';
import moment from 'moment';

interface IProps {
  field: ICustomField;
  disabled?: boolean;
  onChange: (id: number, value?: string) => void;
  inlineLabel?: boolean;
}

export const InputDate = (props: IProps) => {
  const { field, disabled, inlineLabel} = props;

  const handleChange = (date) => {
    props.onChange(field.id, date);
  };

  return (
    <div className={`input-wrapper ${inlineLabel ? 'inline-label' : ''}`}>
      {field.name &&
        <span className='label'>{field.name}</span>
      }

      <DatePicker
        value={field.value ? moment(field.value) : undefined}
        onChange={handleChange}
        disabled={disabled}
        size='small'
        format='DD.MM.YYYY'
      />
    </div>
  );
};
