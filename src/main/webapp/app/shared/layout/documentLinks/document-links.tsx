import {IDocument} from "app/shared/model/document.model";
import {Input} from "antd";
import React from "react";

interface IProps {
  document: IDocument;
  disabled?: boolean;
  onChange?: (id: number, value?: any, error?: string) => void;
  inlineLabel?: boolean;
}

export const DocumentLinks = (props: IProps) => {
  const {document, onChange, disabled, inlineLabel} = props;

  const getInput = () => {
    return (
      <div className={`input-wrapper ${inlineLabel ? 'inline-label' : ''}`}>
        {field.name &&
        <span className='label'>{field.name}</span>
        }

        {disabled ?
          <a
            href={field.value}
            style={{alignSelf: 'flex-start'}}
            rel='noopener noreferrer'
            target='_blank'
          >
            {field.value}
          </a>
          :
          <Input
            value={field.value}
            onChange={handleChange}
            disabled={disabled}
          />
        }
      </div>
    );
  }

  return getInput();
}
