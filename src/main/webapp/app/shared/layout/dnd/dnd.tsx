import React, { useEffect, useState } from 'react';
import './dnd.scss';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Col, Row, Button } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { AddModal } from 'app/shared/layout/dnd/add-modal';

export interface DndEntity {
  id?: any;
  name?: string;
  description?: string;
}

export interface DndEntityHolder {
  id?: any;
  name?: string;
  description?: string;
  content?: DndEntity[];
}

interface IProps {
  disabled: boolean;
  entityHolder: DndEntityHolder;
  fullEntityList: Readonly<DndEntity[]>;
  getFullEntityList?: () => void;
  handleAddEntity: (entityHolderId: number, entityId: string) => void;
  handleDeleteEntity: (entityHolderId: number, entityId: number, position: number) => void;
  handleMoveEntity: (entityHolderId: number, entityId: number, position: number) => void;
}

const getItemStyle = (isDragging, draggableStyle) => ({
  userSelect: 'none',
  padding: '10px 15px',
  margin: `0 0 5px 0`,
  display: 'flex',
  justifyContent: 'space-between',
  background: isDragging ? 'rgba(255, 255, 255, .7)' : 'white',
  ...draggableStyle
});

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export const Dnd = (props: IProps) => {
  const [items, setItems] = useState(props.entityHolder.content || []);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (props.getFullEntityList) {
      props.getFullEntityList();
    }
  }, []);

  useEffect(() => {
    setItems(props.entityHolder.content || []);
  }, [props.entityHolder.content]);

  function onDragEnd(result) {
    if (!result.destination) {
      return;
    }

    props.handleMoveEntity(props.entityHolder.id, +result.draggableId, result.destination.index);
    setItems(reorder(items, result.source.index - 1, result.destination.index - 1));
  }

  function handleAddModalClose() {
    setShowModal(false);
  }

  function handleAddModalOpen() {
    setShowModal(true);
  }

  const handleDeleteBtnClick = (entityId: number, index: number) => () => {
    props.handleDeleteEntity(props.entityHolder.id, entityId, index);
  };

  return (
    <div className='dnd'>
      <Row className='mb-2' style={{width: '430px'}}>
        <Col className='d-flex justify-content-end'>
          <Button onClick={handleAddModalOpen} color='success' disabled={props.disabled}>
            <FontAwesomeIcon icon="plus" /> <Translate contentKey="entity.action.add">Add</Translate>
          </Button>
        </Col>
      </Row>

      {items.length > 0 &&
        <Row>
          <Col>
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId='droppable'>
                {(provided) => (
                  <div
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    className='droppable-wrapper'
                  >
                    {items.map((item: DndEntity, index: number) => (
                      <Draggable
                        key={item.id}
                        draggableId={item.id.toString()}
                        index={index + 1}
                        isDragDisabled={props.disabled}
                      >
                        {(draggableProvided, draggableSnapshot) => (
                          <div
                            ref={draggableProvided.innerRef}
                            {...draggableProvided.draggableProps}
                            {...draggableProvided.dragHandleProps}
                            style={getItemStyle(draggableSnapshot.isDragging, draggableProvided.draggableProps.style)}
                            title={item.description}
                          >
                            <div>
                              <FontAwesomeIcon icon="bars" /> {item.name}
                            </div>

                            <Button
                              close
                              onClick={handleDeleteBtnClick(item.id, index + 1)}
                              disabled={props.disabled}
                            >
                              <span aria-hidden><FontAwesomeIcon icon="trash" size='xs' /></span>
                            </Button>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </Col>
        </Row>
      }

      <AddModal
        entityHolder={props.entityHolder}
        fullEntityList={props.fullEntityList}
        showModal={showModal}
        handleClose={handleAddModalClose}
        handleAddEntity={props.handleAddEntity}
      />
    </div>
  );
};
