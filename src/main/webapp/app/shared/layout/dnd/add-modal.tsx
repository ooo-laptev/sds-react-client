import React, { useEffect, useState } from 'react';
import { DndEntity, DndEntityHolder } from 'app/shared/layout/dnd/dnd';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { AvField } from 'availity-reactstrap-validation';
import { Translate } from 'react-jhipster';

interface IProps {
  fullEntityList: Readonly<DndEntity[]>;
  entityHolder: DndEntityHolder;
  showModal: boolean;
  handleClose: () => void;
  handleAddEntity: (entityHolderId: number, entityId: string) => void;
}

const filterFullEntityList = (fullEntityList: Readonly<DndEntity[]>, entityHolder: DndEntityHolder) => {
  return fullEntityList.filter(entity => entityHolder.content?.every(holderEntity =>
    holderEntity.id !== entity.id
  ));
};

export const AddModal = (props: IProps) => {
  const [entityList, setEntityList] = useState([]);
  const [entityId, setEntityId] = useState(entityList[0]?.id);

  useEffect(() => {
    const filteredEntityList = filterFullEntityList(props.fullEntityList, props.entityHolder);

    setEntityList(filteredEntityList);
    setEntityId(filteredEntityList[0]?.id);
  }, [props.fullEntityList, props.entityHolder]);

  function handleAddEntity() {
    props.handleAddEntity(props.entityHolder.id, entityId);
    props.handleClose();
  }

  function handleChange(e, id) {
    setEntityId(id);
  }

  return (
    <Modal isOpen={props.showModal} toggle={props.handleClose} >
      <ModalHeader toggle={props.handleClose}>
        <Translate contentKey="entity.action.add">Add</Translate>
      </ModalHeader>

        <ModalBody>
          <AvField
            type="select"
            className="form-control"
            name="entity"
            value={entityId}
            onChange={handleChange}
          >
            {entityList.map(entity => (
              <option key={entity.id} title={entity.description} value={entity.id}>{entity.name}</option>
            ))}
          </AvField>
        </ModalBody>

        <ModalFooter>
          <Button onClick={props.handleClose}>
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          &nbsp;
          <Button color="primary" onClick={handleAddEntity} disabled={!entityList.length}>
            <Translate contentKey="entity.action.ok">OK</Translate>
          </Button>
        </ModalFooter>
    </Modal>
  )
};
