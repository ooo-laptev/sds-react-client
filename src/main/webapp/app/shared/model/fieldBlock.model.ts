import { ICustomField } from 'app/shared/model/customfield.model';

export interface IFieldBlock {
  id?: number;
  name?: string;
  description?: string;
}

export interface IFieldBlockLayout {
  id?: number;
  name?: string;
  description?: string;
  content?: ICustomField[];
}

export const defaultValue: Readonly<IFieldBlockLayout> = {
  id: 0,
  name: '',
  description: '',
};
