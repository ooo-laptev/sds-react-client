import { ICustomField } from 'app/shared/model/customfield.model';

export interface IDocumentList {
  content: IDocument[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  sort: {sorted: true, unsorted: false, empty: false}
  totalElements: number;
  totalPages: number;
}

export interface IDocument {
  id?: number;
  created?: Date | null;
  assignedTemplates?: number[];
  defaultCustomFields?: ICustomField[];
}

export const defaultValue: Readonly<IDocument> = {
  id: 0,
  created: null,
  assignedTemplates: []
};
