export interface ICustomField {
  id?: number;
  name?: string;
  description?: string;
  type?: string;
  value: any;
  error?: string;
}

export const defaultValue: Readonly<ICustomField> = {
  id: 0,
  name: '',
  description: '',
  type: '',
  value: null
};
