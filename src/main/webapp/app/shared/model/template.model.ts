import { IFieldBlockLayout } from 'app/shared/model/fieldBlock.model';
import { ICustomField } from 'app/shared/model/customfield.model';

export interface ITemplate {
  id?: number;
  name?: string;
  description?: string;
}

export interface ITemplateLayout {
  id?: number;
  name?: string;
  description?: string;
  content?: IFieldBlockLayout[];
  defaultCustomFields?: ICustomField[];
}

export const defaultValue: Readonly<ITemplateLayout> = {
  id: 0,
  name: '',
  description: '',
  content: [],
  defaultCustomFields: []
};
