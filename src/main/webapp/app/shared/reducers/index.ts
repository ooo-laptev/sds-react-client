import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale from './locale';
import authentication from './authentication';
import applicationProfile from './application-profile';

import administration from 'app/modules/administration/administration.reducer';
import userManagement from 'app/modules/administration/user-management/user-management.reducer';
import register from 'app/modules/account/register/register.reducer';
import activate from 'app/modules/account/activate/activate.reducer';
import password from 'app/modules/account/password/password.reducer';
import settings from 'app/modules/account/settings/settings.reducer';
import passwordReset from 'app/modules/account/password-reset/password-reset.reducer';
import customfieldManagement
  from 'app/modules/administration/sds-configuration/customfield-management/customfield-management.reducer';
import documentManagement
  from 'app/modules/administration/sds-configuration/document-management/document-management.reducer';
import templateManagement
  from 'app/modules/administration/sds-configuration/template-management/template-management.reducer';
import fieldBlockManagement
  from 'app/modules/administration/sds-configuration/fieldBlock-management/fieldBlock-management.reducer';
import documentBrowser from 'app/modules/home/document-browser/document-browser.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const rootReducer = {
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  customfieldManagement,
  documentManagement,
  templateManagement,
  fieldBlockManagement,
  documentBrowser,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
};

export default rootReducer;
